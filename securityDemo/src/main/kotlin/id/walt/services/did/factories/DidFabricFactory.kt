package id.walt.services.did.factories

import id.walt.crypto.Key
import id.walt.model.Did
import id.walt.model.DidUrl
import id.walt.model.did.DidFabric
import id.walt.services.did.DidFabricCreateOptions
import id.walt.services.did.DidOptions
import id.walt.services.did.composers.DidDocumentComposer
import id.walt.services.did.composers.models.DocumentComposerJwkParameter
import id.walt.services.did.composers.models.DocumentComposerKeyJwkParameter
import id.walt.services.key.KeyService
import id.walt.services.keystore.KeyType

class DidFabricFactory(
    private val keyService: KeyService,
    private val documentV1Composer: DidDocumentComposer<DidFabric>,
    private val documentV2Composer: DidDocumentComposer<DidFabric>,
) : DidFactory {
    override fun create(key: Key, options: DidOptions?): Did {
        return when ((options as? DidFabricCreateOptions)?.version ?: 1) {
            1 -> createDidFabricV1(key)
            else -> throw Exception("Did fabric version must be 1 or 2")
        }
    }

    private fun createDidFabricV1(key: Key) = documentV1Composer.make(
        DocumentComposerKeyJwkParameter(
            DidUrl.generateDidFabricV1DidUrl(), keyService.toJwk(key.keyId.id), key
        )
    )

    private fun createDidFabricV2(key: Key) = let {
        val publicKeyJwk = keyService.toJwk(key.keyId.id, KeyType.PUBLIC)
        val publicKeyThumbprint = publicKeyJwk.computeThumbprint()
        documentV2Composer.make(
            DocumentComposerJwkParameter(
                DidUrl.generateDidFabricV2DidUrl(publicKeyThumbprint.decode()), publicKeyJwk
            )
        )
    }

}
