#!/bin/sh

cat <<EOF > security-authorisation-PEP.pu
@startuml

skinparam monochrome false
skinparam shadowing true
skinparam roundcorner 10

actor "Data consumer/provider"

participant "PEP-Proxy"
participant "Target Component API"

group Authorisation DCapBAC - Access to the resource
    "Data consumer/provider" -> "PEP-Proxy" : HTTPS Request (Realtime/Historical component API)+ X-AUTH-TOKEN (Cap.Token)
    "PEP-Proxy" -> "PEP-Proxy" : Validate Cap.Token
    "PEP-Proxy" -> "PEP-Proxy" : Cipher body
    "PEP-Proxy" -> "Target Component API" : Forward Request
    "PEP-Proxy" <- "Target Component API" : Send Response
    "Data consumer/provider" <- "PEP-Proxy" : Send Response
end

@enduml
EOF
plantuml security-authorisation-PEP.pu
