# Security/Trust Demo

Version 1.2

This is a demo for the Security and Trust processes between objects from the VOStack.

Integrated components:
- [X] **Holder**
- [X] **Verifier**
- [X] **PEP-Proxy**
- [X] **Issuer**
- [X] **XACML**
- [X] **Blockchain**

If you have any issues, please contact Alejandro Arias (aarias@odins.es)

## Architecture

![Alt Text](images/SecDemoArquitecture.png)

This is the scenario recreated to test the security and trust between a Composite Virtual Object (cVO) and a Virtual Object (VO). The components that interact in the demo and that we have deployed in docker are the following:

- **Source Code**: this is the main code that is executed in a cVO/VO. The Docker container name of this component is *oidc4vp-demo* and corresponds to the Source Code of the cVO. The functionality of this component is in the file *oidc4vp-demo/securityDemo/src/main/kotlin/id/walt/cli/DemoVO.kt*

- **Holder Component**: this is the component that will act as a wallet. The Holder functionality is to obtain the SIOP request, select the Verifiable Credentials indicated in the SIOP Request, create the Verifiable Presentation and send it to the corresponding API (also indicated in the SIOP Request) to be verified and to obtain the Access Token. As it acts as a wallet, the Holder also stores the Verifiable Credentials and the Decentralized Identifiers (DIDs). The Docker container name for this component is *oidc4vp-holder* and corresponds to the Holder of the cVO. The functionality of this component is in the file *oidc4vp-demo/securityDemo/src/main/kotlin/id/walt/cli/HolderAPI.kt*

- **Verifier Component**: this is the component that will generate the SIOP Request, validate/verify the Verifiable Credentials and generate the Access Token. The Docker container name of this component is *oidc4vp-verfier* and corresponds to the Verifier of the VO1. The functionality of this component is in the file *oidc4vp-demo/securityDemo/src/main/kotlin/id/walt/cli/VerifierAPI.kt*.

- **PEP-Proxy**: this component acts as an entry-point for the VOs/cVOs. The Docker container name of this component is *oidc4vp-proxy* and corresponds to the PEP-Proxy of the VO1. The functionality of this component is in *oidc4vp-demo/PEP-Proxy/PEP-Proxy.py*.


In this scenario, the cVO wants to get data from the VO1. To do that, the cVO will need to present an Access Token to the PEP-Proxy of the VO1. The flow to obtain this Access Token follows the next steps (following the steps in the image above):

**1.** The cVO sends an HTTP Post to his Holder including a JSON with information the Holder will need in the next steps. The structure of the JSON is as follows:

    {
        "device":"http://oidc4vp-proxy:1027", # PEP-Proxy's API of the VO1 from the cVO wants to get the data

        "method":"GET", # HTTP Method the cVO wants to use for the interaction with the VO1 (GET/POST/
        PUT),

        "resource":"/version", # VO1 Resource's the cVO wants to get data from,
        
        "requester":"192.168.1.1", # cVO IP. Security related. 

    }

**2.0** The Holder starts the process to obtain an Access Token. First, the Holder parses the JSON received from the cVO and obtains all data. With the data obtained, the Holder can now make an HTTP GET to the VO1's PEP-Proxy to obtain the SIOP Request.

**2.1** The PEP-Proxy forwards the request from the cVO's Holder to the VO1's Verifier, which generates the SIOP Request and sends it to the cVO's Holder. 
This SIOP request contains info about what credentials the cVO must present to the VO's verifier or the redirectURI where the Holder needs to send the Verifiable Presentation to be verified.

**3.0** The Holder receives and parses the SIOP Request, selects the corresponding Verifiable Credential/s and makes the Verifiable Presentation. Once the VP is made, the Holder makes an HTTP POST to the redirectURI indicated in the SIOP Request, including the Verifiable Presentation it just created and it also adds some headers (method, resource, requester) with the information from the JSON it received in Step 1 from the cVO.

**3.1** The VO1's PEP-Proxy forwards the HTTP Post sent by the cVO's Holder to the VO1's Verifier. The Verifier validates and verifies the Verifiable Presentation and, if the validation is correct, the Verifier generates the Access Token and sends it to the cVO's Holder.

**4.0** The cVO stores the Access Token and the cVO makes an HTTP GET to the resource it initially wanted to get the data from including the Access Token in the 'x-auth-header'.

**4.1** The PEP-Proxy validates the Access Token and if it is validated, the PEP-Proxy forwards the request to the API the cVO wants to get data from.


## Launch Demo

To launch the Security/Trust demo you will have to clone this repository in your local machine.

Once you do that, execute the following commands:

```
docker-compose build
```

Wait until the build is finished (it may take some time when you execute it for the first time, do not worry).

```
docker-compose up -d
```

The flag '-d' is for running the containers in detached mode (in the background) so you do not have your terminal filled with prints and logs.

Once these commands have been executed, you can see the **logs** from each component deployed in docker with the following command:

```
docker-compose logs -f containerName
```

Where *containerName* is the name of the container you want to see the logs:

- oidc4vp-demo — for the cVO component.

- oidc4vp-holder — for the Holder component.

- oidc4vp-verifier — for the Verifier component.

- oidc4vp-proxy — for the PEP-Proxy component.

## EXPECTED RESULTS

If everything is working fine, you should be able to see the following messages in the logs of 'oidc4vp-demo':

![Alt text](images/ExpectedResults.png)

We are testing the implementation of regular expressions in Access Tokens so that different resources can be accessed with the same Access Token. In this case, we are simulating that the cVO wants to consume the resources 'http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a1111' and 'http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a2222' from VO1, so the cVO requests an Access Token for 'http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a.*'. Once the Access Token is received, the cVO makes 3 requests to resources from VO1. As you can see in the image above, the cVO is authorized to consume resources from '(...)a1111' and '(...)a2222' but it is not authorized to consume data from '(...)b1111'. Please note that the data received from '(...)a1111' and '(...)a2222' resources is empty because it is not implemented, we just check if the cVO can access to those resources with the requested Access Token.

If you decode the Access Token (you can just copy/paste the token in http://jwt.io), you should be able to see something similar to this:

![Alt text](images/AccessToken.png)

Also, for the 'oidc4vp-proxy' logs, you should be able to see this:

![Alt text](images/PEP-ProxyLogs.png)

In the image above, you can see that the PEP-Proxy (the component that verifies the Access Token, among other things) authorizes the two first requests and makes the GET to the indicated resource (it receives a 404 error because the resource is not implemented) but it does not authorize the last request to the resource '(...)b1111'.

## Issuer

To test the Issuer, you have to execute the following commands:

Move to the securityDemo folder:
```
cd securityDemo
```

Build the components:
```
./ssikit.sh build-st
```

In one terminal, launch:
```
./ssikit.sh issuer
```

In other terminal, launch:
```
./ssikit.sh testIssuer
```

In the following flow diagram, you can see the exchanges between the Issuer and the Holder to obtain the Verifiable Credential using the OIDC4CI protocol: 


![Alt text](images/OIDC4CI-FLOW.drawio.png)

## XACML

Work in progress... :warning: :construction_worker: :construction:
