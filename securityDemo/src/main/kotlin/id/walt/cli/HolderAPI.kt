package id.walt.cli

import com.github.ajalt.clikt.core.CliktCommand
import io.ktor.server.application.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.request.receiveText
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.request.post
import io.ktor.client.statement.*
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.*
import id.walt.services.oidc.OIDC4VPService
import java.net.URI
import id.walt.services.oidc.OIDCUtils
import id.walt.common.KlaxonWithConverters
import id.walt.common.prettyPrint
import id.walt.custodian.Custodian
import id.walt.credentials.w3c.toVerifiablePresentation
import com.nimbusds.oauth2.sdk.ResponseMode
import id.walt.services.oidc.CompatibilityMode
import id.walt.credentials.w3c.PresentableCredential
import java.util.*
import id.walt.services.did.DidService
import com.nimbusds.oauth2.sdk.http.HTTPRequest
import kotlinx.serialization.json.*
import id.walt.common.printLog
import com.google.gson.Gson
import kotlinx.serialization.encodeToString
import id.walt.model.oidc.*
import id.walt.services.jwt.JwtService
import com.nimbusds.jwt.JWTClaimsSet
import java.security.SecureRandom
import id.walt.credentials.w3c.toVerifiableCredential
import id.walt.common.createDid
import id.walt.common.genKeys


/** CLI Command to run the walt.id SSI KIT as RESTful service. */
class HolderAPI :
        CliktCommand(
                name = "holder",
                help =
                        """
         """
        ) {

        // ISSUER
        val ENDPOINT_AUTH = System.getenv("issuer_host") + "/auth"
        val ENDPOINT_CODE = System.getenv("issuer_host") + "/code"
        val ENDPOINT_TOKEN = System.getenv("issuer_host") + "/token"
        val ENDPOINT_CREDENTIAL = System.getenv("issuer_host") + "/credential"
        // Salida mas legible
        val green = "\u001B[32m"
        val red = "\u001B[31m"
        val reset = "\u001B[0m"
        // KEY
        val DEFAULT_ALGORITHM = "Secp256k1"
        // DID
        val DEFAULT_DID_METHOD = "fabric"
        lateinit var DID: String

    override fun run() {
        // Create DID
        val keyAlias = genKeys(DEFAULT_ALGORITHM)
        printLog(keyAlias)
        createDid(DEFAULT_DID_METHOD, keyAlias)
        DID = DidService.getDid()

        embeddedServer(Netty, host = "0.0.0.0", port = 8080) {
            
            // JSON elements
            var device: String? = ""
            var method: String? = ""
            var resource: String? = ""
            var requester: String? = ""
            var url: String? = ""
            val logs: Boolean = System.getenv("logs").toBoolean()
    
            routing { get("/") { call.respondText("Holder's API") } };

            routing {
                post("/auth") {
                    // Receive JSON
                    val json = call.receiveText()

                    // Obtain data from JSON
                    val datos = Json.parseToJsonElement(json).jsonObject
                    device = datos["device"]?.jsonPrimitive?.contentOrNull
                    method = datos["method"]?.jsonPrimitive?.contentOrNull
                    resource = datos["resource"]?.jsonPrimitive?.contentOrNull
                    requester = datos["requester"]?.jsonPrimitive?.contentOrNull

                    url = device + resource
                    
                    printLog("1. Data received in /auth endpoint: $json\n")

                    if (logs) {
                        printLog("device: $device\nmethod: $method\nresource: $resource\nrequester: $requester\nurl: $url\n")
                    }

                    printLog("2. SENT -> GET Request to $device/obtainVP\n")
                    // Send GET Request to Verifier
                    val res = HTTPRequest(HTTPRequest.Method.GET, URI.create(device + "/obtainVP")).apply{
                        setHeader("Device", device)
                        followRedirects = false
                    }.send()
          
                    // Get SIOP Request from Verifier Response
                    val siopRequest = res.location.toString()

                    printLog("3. RECEIVED -> SIOP Request\n")
                    if (logs) {
                        printLog("Response from Verifier: $siopRequest\n")
                    }
                    

                    // Parse the SIOP Request
                    var selectedVC = mutableListOf<String>()
                    val req = OIDC4VPService.parseOIDC4VPRequestUri(URI.create(siopRequest))
                    if (req == null){
                        printLog("Error parsing SIOP request")
                    } else {
                        val presentationDefinition = OIDC4VPService.getPresentationDefinition(req)

                        if (logs){
                            printLog("Presentation requirements")
                            printLog(KlaxonWithConverters().toJsonString(presentationDefinition).prettyPrint())
            
                            printLog("-------------------------------")
                            printLog("Matching credentials by input descriptor id:\n")
                        }
                        
                        val credentialMap = OIDCUtils.findCredentialsFor(OIDC4VPService.getPresentationDefinition(req))
                        //credentialMap.keys.forEach { inputDescriptor ->
                        //    selectedVC.add("${credentialMap[inputDescriptor]?.joinToString(", ") ?: "<none>"}")
                        //}
                        credentialMap.keys.forEach { inputDescriptor ->
                            val valueToAdd = credentialMap[inputDescriptor]?.joinToString(", ")
                            if (valueToAdd != null && valueToAdd!=""){
                                selectedVC.add(valueToAdd)
                                printLog("Added value \"$valueToAdd\"")
                            } else {
                                printLog("No value found for inputDescriptor: $inputDescriptor")
                            }
                        }

                        if (selectedVC.isEmpty()){
                            // Request Credential
                            val vcId: String? = presentationDefinition?.input_descriptors?.firstOrNull()?.constraints?.fields
                                ?.firstOrNull { it.filter?.get("const") != null }
                                ?.filter?.get("const") as? String
                            if (vcId != null) {
                                printLog("VC id requested by target Verifier: $vcId")
                            } else {
                                printLog("Could not retrieve \"const\" field from presentation definition")
                            }
                            
                            call.respond(HttpStatusCode.NotFound, "$vcId")
                        }
                        printLog("Credential matched: $selectedVC")
                    }

                    if (logs) {
                        printLog("DID: $DID\n")
                    }
                    
                    // Create presentation response
                    val req_vp = OIDC4VPService.parseOIDC4VPRequestUri(URI.create(siopRequest))
                    val nonce = req_vp.getCustomParameter("nonce")?.firstOrNull()
                    var vp = """
                        {"type":["VerifiablePresentation"],"@context":["https://www.w3.org/2018/credentials/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"urn:uuid:62354a0c-9e52-478c-9f74-e42247460412","proof":{"type":"JsonWebSignature2020","creator":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R","created":"2023-10-27T11:50:52Z","proofPurpose":"authentication","verificationMethod":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R#9339eefce5f54e5b88d1d1a56e55228e","jws":"eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFUzI1NksifQ..BAgI23wgyVd7dz8RS0mTuiBelCxplKUo9-J0iicAOfiQZbqefbX-SgBR47HJxMvyFNevwzRyZa8UMBP9Z_3lGw","nonce":"05443caf-b6e2-4deb-a251-7da643de1fb2"},"holder":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R","verifiableCredential":[{"type":["VerifiableCredential","VerifiableAttestation","demoTemplate"],"@context":["https://www.w3.org/2018/credentials/v1","https://w3id.org/security/suites/jws-2020/v1"],"id":"urn:uuid:14acee28-8941-4bf1-a180-17a1ce4952a2","issuer":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R","issuanceDate":"2023-10-16T14:10:41Z","issued":"2024-01-16T14:10:41Z","validFrom":"2023-10-16T14:10:41Z","proof":{"type":"JsonWebSignature2020","creator":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R","created":"2023-10-16T14:10:41Z","proofPurpose":"assertionMethod","verificationMethod":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R#9339eefce5f54e5b88d1d1a56e55228e","jws":"eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFUzI1NksifQ..T93TePnQZLhSuDlSmHD06nX0N1dLov90aRwkLPFZCDvMG7GlS0uBcrXjBlNZVkVW3AuncFC-vo3wpLODqy7ITw"},"credentialSchema":{"id":"https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/VerifiableId.json","type":"FullJsonSchemaValidator2021"},"credentialSubject":{"id":"did:ebsi:zq3kRZe5vsTWHchrUVPbX3R","component":"Composite Virtual Object","deployed":"docker","voId":"cV01"},"evidence":[{"type":["DocumentVerification"],"verifier":"did:ebsi:2A9BZ9SUe6BatacSpvs1V5CdjHvLpQ7bEsi2Jb6LdHKnQxaN"}]}]}
                        """.toVerifiablePresentation()
                    try {
                        vp = Custodian.getService().createPresentation(
                        vcs = selectedVC.map { Custodian.getService().getCredential(it)?.let { PresentableCredential(it) } ?: throw Exception("Credential with given ID $it not found") },
                        holderDid = DID,
                        challenge = nonce,
                        ).toVerifiablePresentation()
                    } catch (e: Exception){
                        printLog("An error has occured during the generation of the Verifiable Presentation: ${e.message}")
                    }
                    
                    val resp = OIDC4VPService.getSIOPResponseFor(req_vp, DID, listOf(vp))

                    if (logs) {
                        printLog("Presentation response:")
                        printLog("${resp.toFormParams().prettyPrint()}\n")
                    }
                    
                    printLog("3. SENT -> Verifiable Presentation + ID_Token to ${req_vp.redirectionURI}")
                    if (setOf(ResponseMode.FORM_POST, ResponseMode("post")).contains(req_vp.responseMode)){
                        val result = OIDC4VPService.postSIOPResponse(req_vp, resp, CompatibilityMode.OIDC, method, url, requester)
                        if (result != "") {
                            printLog("\n3. RECEIVED -> Access Token: $result\n")
                            call.respond(HttpStatusCode.OK, "$result")
                        } else {
                            printLog("3. RECEIVED -> UNAUTHORIZED")
                            call.respond(HttpStatusCode.Unauthorized, "Unauthorized to obtain Access Token")
                        }
                    } else {
                        if (logs){   
                            printLog("Redirect to:")
                            printLog(
                                "${req_vp.redirectionURI}${
                                    when (req_vp.responseMode){
                                        ResponseMode.FRAGMENT -> "#"; else -> "?"
                                    }
                                }${resp.toFormBody()}"
                            )
                        }
                    }

                    call.respond(HttpStatusCode.OK, "Works")
                }
            }

            routing {
                post("/obtainCredential"){
                    
                    // Receive JSON
                    val attributes = call.receiveText()

                    // Obtain data from JSON
                    val attributesJSON = Json.parseToJsonElement(attributes).jsonObject
                    val uuid = attributesJSON["uuid"]?.jsonPrimitive?.contentOrNull
                    val type = attributesJSON["type"]?.jsonPrimitive?.contentOrNull
                    val description = attributesJSON["description"]?.jsonPrimitive?.contentOrNull
                    val vcTemplate = attributesJSON["vcTemplate"]?.jsonPrimitive?.contentOrNull

                    printLog("Attributes received in endpoint /obtainCredential: ")
                    printLog("uuid: $uuid")
                    printLog("type: $type")
                    printLog("description: $description")
                    
                    val client =
                    HttpClient() {
                        install(ContentNegotiation) { json() }
                        expectSuccess = false
                    }
                        
                    // Select Credential to be issued
                    val credentialType: String = vcTemplate?. let{it} ?: ""

                    // Get authentication code
                    val uri = push_OIDC_auth_request(client)

                    val state: String = generarStateAleatorio()

                    println("uri: " + uri)
                    println("state: " + state)

                    // Get pre_auth JWT
                    val text = get_JWT_and_STATE(client, state, uri)

                    val parts = text.split(":")
                    val jwt = parts[0]
                    val stateResponse = parts[1]

                    println("JWT:" + jwt)
                    println("STATE:" + stateResponse)

                    if(state != stateResponse){
                        println("")
                        println("[!] States do not match! The Issuer response is not valid")
                    } 
                    else{
                    
                        println("")
                        println("[+] States match! The Issuer response is valid")

                        // Get access token for credential issuance
                        val token = get_Access_Token(client, jwt)
                        println(token)

                        // Get Credential
                       
                        val cred = getCredential(client, token, credentialType, attributes)
                        println(cred)
                        
                        // Store credential
                        val credential = cred.toVerifiableCredential()
                        val storeId = credential.id ?: "custodian#${UUID.randomUUID()}"
                        Custodian.getService().storeCredential(storeId, credential)
                        println("Credential stored as $storeId")
                        
                    }

                    client.close()

                    call.respond(HttpStatusCode.OK)
                }
            }

            routing {
                get("/isVCStored"){
                    val myCredentials = Custodian.getService().listCredentials()
                    if (myCredentials.isEmpty()){
                        call.respond(HttpStatusCode.NotFound)
                    } else {
                        call.respond(HttpStatusCode.OK)
                    }

                }
            }
        }.start(wait = true)
    }

    suspend fun getCredential(client: HttpClient, token: String, credentialType: String, attributes: String): String {
        println("")
        println(green + "[+] GET credential" + reset)
        println("")

        val gson = Gson()
        val jsonObject = gson.fromJson(token, Map::class.java)
        var access_token = jsonObject["access_token"] as String
        println("access token: " + access_token)
        var valor_nonce = jsonObject["c_nonce"] as String
        println("c_nonce: " + valor_nonce)
        val template = credentialType
        val nonce_signed = generateDidProof(DID,valor_nonce)

        val response = client.get(ENDPOINT_CREDENTIAL) {
            url {
                parameters.append("nonce_signed", nonce_signed.toString())
                parameters.append("template", template)
                parameters.append("token", access_token)
                parameters.append("attributes", attributes)
            }
        }


        //println("public key: "+key)
        println("acces_token: " + access_token)
        println("nonce_signed: " + nonce_signed)
        println("nonce: " + valor_nonce)

        val credential: String = response.bodyAsText()
        return credential
    }

    suspend fun get_Access_Token(client: HttpClient, jwt: String): String {
        println("")
        println(green + "[+] GET access Token" + reset)
        println("")

        var response = client.get(ENDPOINT_TOKEN) {
            url {
                parameters.append("jwt", jwt)
            }
        }

        val token: String = response.bodyAsText()
        return token
    }

    suspend fun push_OIDC_auth_request(client: HttpClient): String{
        println("")
        println(green + "[+] PUSH OIDC auth request" + reset)
        println("")

        var response = client.get(ENDPOINT_AUTH) {

        }

        val uri: String = response.bodyAsText()

        return uri

    }

    suspend fun get_JWT_and_STATE(client: HttpClient, state: String, uri: String): String{

        println("")
        println(green + "[+] GET JWT and STATE" + reset)
        println("")


        var response = client.get(ENDPOINT_CODE) {
            url {
                parameters.append("uri", uri)
                parameters.append("state", state)
            }
        }

        val text: String = response.bodyAsText()
        return text

    }

    fun generateDidProof(did: String, nonce: String): JwtProof {
        val didObj = DidService.load(did)
        val vm = (didObj.authentication ?: didObj.assertionMethod ?: didObj.verificationMethod)?.firstOrNull()?.id ?: did
        return JwtProof(
            jwt = JwtService.getService().sign(
                vm,
                JWTClaimsSet.Builder()
                    .issuer(did) // Utiliza el DID como emisor en lugar de issuer.client_id o did
                    .issueTime(Date())
                    .claim("nonce", nonce)
                    .build().toString()
            ),
        )
    }


    fun generarStateAleatorio(): String {
        val secureRandom = SecureRandom()
        val bytes = ByteArray(32)
        secureRandom.nextBytes(bytes)

        val base64String = Base64.getUrlEncoder().withoutPadding().encodeToString(bytes)
    
        val state = base64String.substring(0, 16) + "-" + base64String.substring(16)
        
        return state
    }
}

