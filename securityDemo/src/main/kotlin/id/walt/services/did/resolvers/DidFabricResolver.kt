package id.walt.services.did.resolvers

import id.walt.model.Did
import id.walt.model.DidUrl
import id.walt.model.did.DidFabric
import id.walt.services.did.DidFabricResolveOptions
import id.walt.services.did.DidOptions
import id.walt.services.did.composers.DidDocumentComposer
import id.walt.services.did.composers.models.DocumentComposerJwkParameter
import id.walt.services.ecosystems.essif.TrustedIssuerClient
import id.walt.services.key.KeyService
import io.ipfs.multibase.Multibase
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class DidFabricResolver(
    private val httpClient: HttpClient,
    private val keyService: KeyService,
    private val fabricV2documentComposer: DidDocumentComposer<DidFabric>,
) : DidResolverBase<DidFabric>() {

    private val didRegistryPath = "did-registry/${TrustedIssuerClient.apiVersion}/identifiers"
    override fun resolve(didUrl: DidUrl, options: DidOptions?) = (options as? DidFabricResolveOptions)?.takeIf {
        it.isRaw
    }?.let { resolveDidFabricRaw(didUrl.did) }?:resolveFabric(didUrl)

    private fun resolveFabric(didUrl: DidUrl) = when (Multibase.decode(didUrl.identifier).first().toInt()) {
        1 -> resolveDidFabricV1(didUrl)
        //2 -> resolveDidFabricV2(didUrl)
        else -> throw IllegalArgumentException("did:fabric must have version 1 or 2")
    }

    private fun resolveDidFabricV1(didUrl: DidUrl): DidFabric = runBlocking {

        log.debug { "Resolving DID ${didUrl.did}..." }

        var didDoc: String
        var lastEx: ClientRequestException? = null

        for (i in 1..5) {
            try {
                log.debug { "Resolving did:fabric at: ${TrustedIssuerClient.domain}/$didRegistryPath/${didUrl.did}" }
                didDoc = httpClient.get("${TrustedIssuerClient.domain}/$didRegistryPath/${didUrl.did}").bodyAsText()
                log.debug { "Result: $didDoc" }
                return@runBlocking Did.decode(didDoc)!! as DidFabric
            } catch (e: ClientRequestException) {
                log.debug { "Resolving did fabric failed: fail $i" }
                delay(1000)
                lastEx = e
            }
        }
        log.debug { "Could not resolve did fabric!" }
        throw lastEx ?: Exception("Could not resolve did fabric!")
    }

    //private fun resolveDidFabricV2(didUrl: DidUrl): DidFabric {
    //    val jwk = keyService.toJwk(didUrl.did)
    //    if (DidUrl.generateDidFabricV2DidUrl(jwk.computeThumbprint().decode()).identifier != didUrl.identifier) {
    //        throw IllegalArgumentException("Public key doesn't match with DID identifier")
    //    }
    //    return fabricV2documentComposer.make(DocumentComposerJwkParameter(didUrl, jwk))
    //}

    private fun resolveDidFabricRaw(did: String): Did = runBlocking {
        log.debug { "Resolving DID $did" }
        val didDoc = httpClient.get("${TrustedIssuerClient.domain}/$didRegistryPath/$did").bodyAsText()
        log.debug { didDoc }
        Did.decode(didDoc) ?: throw Exception("Could not resolve $did")
    }
}
