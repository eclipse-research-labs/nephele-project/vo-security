package id.walt.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.sun.jdi.Location
import com.google.common.net.HttpHeaders
import io.ktor.server.application.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.request.receiveText
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.request.post
import io.ktor.client.statement.*
import io.ktor.http.HttpStatusCode
import id.walt.services.oidc.OIDC4VPService
import java.net.URI
import com.nimbusds.openid.connect.sdk.Nonce
import com.nimbusds.oauth2.sdk.ResponseType
import com.nimbusds.oauth2.sdk.ResponseMode
import com.nimbusds.oauth2.sdk.Scope
import id.walt.model.dif.PresentationDefinition
import id.walt.model.dif.InputDescriptor
import id.walt.model.dif.InputDescriptorConstraints
import id.walt.model.dif.InputDescriptorField
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.nimbusds.oauth2.sdk.id.State
import id.walt.services.oidc.OidcSchemeFixer.unescapeOpenIdScheme
import java.net.URLDecoder
import id.walt.services.oidc.OIDCUtils
import id.walt.credentials.w3c.VerifiablePresentation
import id.walt.credentials.w3c.VerifiableCredential
import id.walt.auditor.Auditor
import id.walt.auditor.PolicyRegistry
import id.walt.common.resolveContent
import java.util.*
import id.walt.services.jwt.JwtService
import id.walt.model.oidc.SelfIssuedIDToken
import java.time.Instant
import id.walt.services.did.DidService
import java.time.Duration
import id.walt.common.printLog
import io.ktor.serialization.kotlinx.json.*
import kotlin.text.Regex
import id.walt.common.createDid
import id.walt.common.genKeys
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import id.walt.services.ecosystems.fabric.ATHIST
import com.nimbusds.jwt.SignedJWT

/** CLI Command to run the walt.id SSI KIT as RESTful service. */
class VerifierAPI :
        CliktCommand(
                name = "verifier",
                help =
                        """Función HTTP para verificacion
                    de Verifiable Credentials" 
         """
        ) {
            
            // Global variables and constants
            var actualState: String? = null // For correlation between SIOP Request and SIOP Response
            val MAX_TIME = 60 // Expiration time between sending the SIOP request and receiving SIOP Response
            var requestTime: Long = 0 // Aux varibale to store time instant at which the SIOP request is sent
            // XACML
            val XACML_PDP = System.getenv("xacml_pdp")
            val XACML_DOMAIN = System.getenv("xacml_domain")
            // JWT SERVICE
            private val jwtService = JwtService.getService()
            // KEY
            val DEFAULT_ALGORITHM = "Secp256k1"
            // DID
            val DEFAULT_DID_METHOD = "fabric"
            lateinit var DID: String
            // LOGS
            val logs: Boolean = System.getenv("logs").toBoolean()
           
            override fun run() {
                val keyAlias = genKeys(DEFAULT_ALGORITHM)
                printLog(keyAlias)
                createDid(DEFAULT_DID_METHOD, keyAlias)
                DID = DidService.getDid()

                embeddedServer(Netty, host = "0.0.0.0", port = 8080) {

                    routing { get("/") { call.respondText("Hello, world!") } };

                    routing {
                        get("/obtainVP") {
                            // Generate random value for Nonce and State
                            val randomValue = UUID.randomUUID().toString()

                            // Generate SIOP Request
                            //val client_url = "https://wallet.walt.id/api/siop/initiatePresentation/" // Check
                            val response_type = "id_token"
                            val response_mode = "form_post"
                            val nonce = "$randomValue"
                            val scope: String? = null
                            val presentationDefinitionUrl: String? = null
                            val credentialTypes = listOf("NepheleId")
                            val state = "$randomValue"
                            actualState = state

                            val device = call.request.headers["Device"]

                            val siopRequest = OIDC4VPService.createOIDC4VPRequest(
                                //wallet_url = client_url,
                                wallet_url = "openid://",
                                redirect_uri = URI.create(device + "/verifyVP"),
                                nonce = nonce?.let { Nonce(it) } ?: Nonce(),
                                response_type = ResponseType.parse(response_type),
                                response_mode = ResponseMode(response_mode),
                                scope = scope?.let { Scope(scope) },
                                presentation_definition = if (scope.isNullOrEmpty() && presentationDefinitionUrl.isNullOrEmpty()) {
                                PresentationDefinition("1",
                                    input_descriptors = credentialTypes?.map { credType ->
                                        InputDescriptor(
                                            "1",
                                            constraints = InputDescriptorConstraints(
                                                listOf(
                                                    InputDescriptorField(
                                                        listOf("$.type"),
                                                        "1",
                                                        filter = JsonObject(mapOf("const" to credType))
                                                    )
                                                )
                                            )
                                        )
                                    } ?: listOf())
                                } else {
                                    null
                                },
                                presentation_definition_uri = presentationDefinitionUrl?.let { URI.create(it) },
                                state = state?.let { State(it) }
                            )

                            if (logs) {
                                printLog("${siopRequest.toURI().unescapeOpenIdScheme()}")
                            }
                            // Send SIOP Request to Holder
                            call.response.header(HttpHeaders.LOCATION, "${siopRequest.toURI().unescapeOpenIdScheme()}") // Add Location header to Response
                            call.respond(
                                HttpStatusCode.Found,
                                HttpHeaders.LOCATION
                            )

                            requestTime = System.currentTimeMillis()
                        }
                    };

                    routing{
                        post("/verifyVP"){
                            // Check if time between Request and Response has expired
                            val responseTime = System.currentTimeMillis()

                            if ((responseTime - requestTime) / 1000 > MAX_TIME){
                                //TODO Send Error Response and exit
                                printLog("Time expired\n")
                            } else {
                                //printLog("In time!\n")
                            }

                            // Get body from Request
                            var requestContent = call.receiveText() 
                            if (logs){
                                printLog("$requestContent\n")
                            }
                            
                            requestContent = requestContent.trimIndent()

                            var jsonString = URLDecoder.decode(requestContent, "UTF-8")

                            // Replace special characters
                            jsonString = jsonString.replace("%40", "@")
                                    .replace("%2F", "/")
                                    .replace("%2C", ",")
                                    .replace("%3A", ":")
                                    .replace("%3D", "=")
                                    .replace("%22", "\"")
                                    .replace("%7B", "{")
                                    .replace("%7D", "}")
                                    .replace("%5B", "[")
                                    .replace("%5D", "]")
                                    .replace("%2B", " ")
                                    .replace("%20", " ")  // Replace '+' for blank spaces

                            var vpToken: String = ""
                            var presentation_submission: String? = null
                            var idToken: String = ""
                            var stateResponse: String? = null

                            var parts = jsonString.split('&')
                            parts.forEach { part ->
                                val (clave, valor) = part.split('=')
                                when (clave){
                                    "vp_token" -> vpToken = valor
                                    "presentation_submission" -> presentation_submission = valor
                                    "id_token" -> idToken = valor
                                    "state" -> stateResponse = valor
                                }
                            }

                            if (logs) {
                                printLog("vp_token: $vpToken\n")
                                printLog("presentation_submission: $presentation_submission\n")
                                printLog("id_token: $idToken\n")
                                printLog("state: $stateResponse\n")
                            }
                            

                            // Check if the response state param matches the request state param
                            if (stateResponse == actualState){
                                if (logs){
                                    printLog("The response is correct! Starting verification process...\n")
                                }
                            } else {
                                printLog("The response is not valid or have expired\n")
                                //TODO Send Error Response and exit
                            }

                            // Get VerifiablePresentation object from vp_token
                            val vps = OIDCUtils.fromVpToken(vpToken)
                            var vp: VerifiablePresentation = vps.first()
                            if (logs){
                                printLog("VerifiablePresentation: $vp\n")
                            }
                            
                            /*if (vps.isNotEmpty()){
                                vp = vps.first()
                                printLog("VerifiablePresentation: $vp\n")
                            }*/

                            // Verification process        
                            val vcs = vp.verifiableCredential
                            var vc: VerifiableCredential = VerifiableCredential()
                            if (vcs.isNullOrEmpty()){
                                printLog("Could not retrieve any Verifiable Credential from Verifiable Presentation\n")
                            } else{
                                vc = vcs.first()
                                if (logs){
                                    printLog("VerifiableCredential: $vc")
                                }

                            }

                            // Convert Verifibale Credential to JSON
                            val json = vc.toJson()

                            // Parse JSON to obtain "voId" attribute
                            val parser = Parser.default()
                            val jsonStr = json.toString()
                            val parsedJson = parser.parse(StringBuilder(jsonStr)) as JsonObject
                            val credentialSubject = parsedJson.obj("credentialSubject")
                            val voId = credentialSubject?.string("uuid")
                            val subjectDid = credentialSubject?.string("id")

                            // Obtain DID from ID_Token
                            val idToken_did = getDIDfromIDToken(idToken)
        

                            // Verify ID_Token
                            if (jwtService.verify(idToken).verified && subjectDid == idToken_did){
                                printLog("ID_TOKEN Verified!\n")
                            } else {
                                printLog("ID_TOKEN not Verified!")
                                call.respond(HttpStatusCode.Unauthorized, "Could not verify ID_TOKEN")
                            }


                            var policies: Map<String, String?> = emptyMap<String, String?>()

                            var usedPolicies = policies.ifEmpty { mapOf(PolicyRegistry.defaultPolicyId to null) }
                            usedPolicies = usedPolicies.toMutableMap()
                            usedPolicies["IssuedDateBeforePolicy"] = null
                            //usedPolicies["ExpirationDateAfterPolicy"] = null
                            when {
                                usedPolicies.keys.any { !PolicyRegistry.contains(it) } -> throw NoSuchElementException(
                                    "Unknown verification policy specified: ${
                                        usedPolicies.keys.minus(PolicyRegistry.listPolicies().toSet()).joinToString()
                                    }"
                                )
                            }

                            var verified = false
                            try {
                                val verificationResult = Auditor.getService()
                                    .verify(
                                        vc,
                                        usedPolicies.entries.map { PolicyRegistry.getPolicyWithJsonArg(it.key, it.value?.ifEmpty { null }?.let {
                                resolveContent(it)
                                }) })

                                if (logs){
                                    echo("\nResults:\n")

                                    verificationResult.policyResults.forEach { (policy, result) ->
                                        echo("$policy:\t $result")
                                    }
                                    echo("Verified:\t\t ${verificationResult.result}\n")
                                }
                                verified = verificationResult.result
                            } catch (e: Exception) {
                                verified = true
                                printLog("An error has occured during the verification of the VP")
                            }

                            if (verified){
                                printLog("Credentials Verified! Checking authorization with XACML...\n")
                            } else {
                                printLog("Credentials not Verified!\n")
                                call.respond(HttpStatusCode.Unauthorized, "Invalid Credentials")
                            }

                            val client =
                                HttpClient() {
                                    install(ContentNegotiation) { json() }
                                    expectSuccess = false
                                }
                            
                            val url = call.request.headers["url"]
                            val method = call.request.headers["method"]
                            val requester = call.request.headers["requester"]
                            
                            // Start authorization process with XACML
                            var authorized = true
                            val xacml: Boolean = System.getenv("xacml").toBoolean()
                            if (xacml) {
                                authorized = xacml(client, voId, url, method)
                                printLog("Using XACML\n")
                            } else {
                                printLog("Not using XACML\n")
                            }

                            if (authorized && verified){
                                printLog("Authorized! Sending Access Token to Holder\n")
                                
                                val subjectId = vc.subjectId ?: throw NoSuchElementException("No subject DID found for VC or VP")
                                val expiration_time: Long = System.getenv("expiration_time").toLong() // Get expiration_time from environment variable

                                val accessToken =  SelfIssuedIDToken(
                                    subject = subjectId,
                                    issuer = DID,
                                    client_id = null,
                                    nonce = null,
                                    expiration = Instant.now().plus(Duration.ofMinutes(expiration_time)),
                                    requester = requester,
                                    method = method,
                                    url = url,
                                    _vp_token = null
                                ).sign()
                                printLog("Access Token: $accessToken")
                                
                                // Store historical in blokchain
                                //if (method != null && url != null && requester != null) {
                                //  storeHist(method, url, requester, accessToken)
                                //}

                                call.respond(HttpStatusCode.OK, accessToken)      
                            } else {
                                printLog("Unauthorized in XACML\n")
                                call.respond(HttpStatusCode.Unauthorized, "Unauthorized in XACML")
                            }

                        }
                    };

                    routing{
                        get("/secret"){
                            val directory = File("data/key")

                            // Obtain secret
                            val files = directory.listFiles { _, name -> name.endsWith(".enc-pubkey") }
                            var secret = ""
                            if (files != null) {
                                for (file in files) {
                                    secret = file.readText()
                                }
                            } else{
                                printLog("Couldn't obtain secret from $directory")
                                call.respond(HttpStatusCode.NotFound, "Couldn't find secret in Verifier")
                            }

                            // Obtain algorithm
                            //val algFiles = directory.listFiles { _, name -> name.endsWith(".meta") }
                            //var algorithm = ""
                            //if (algFiles != null) {
                            //    for (file in algFiles) {
                            //         val content = file.readText()
                            //         algorithm = content.substringAfter('_').substringBefore(';')
                            //         printLog("Algorithm ----> $algorithm")
                            //    }
                            //}

                            call.respond(HttpStatusCode.OK, secret)
                        }
  
                    }
                }.start(wait = true)
            }

            fun getDIDfromIDToken(idToken: String): String {
                val jwt = SignedJWT.parse(idToken)
                val did = jwt.jwtClaimsSet.issuer

                return did
            }

            suspend fun xacml(client: HttpClient, subject: String?, resource: String?, action: String?): Boolean {
                val req = """
                <Request xmlns="urn:oasis:names:tc:xacml:2.0:context:schema:os">
                    <Subject SubjectCategory="urn:oasis:names:tc:xacml:1.0:subject-category:access-subject">
                        <Attribute AttributeId="urn:ietf:params:scim:schemas:core:2.0:id" DataType="http://www.w3.org/2001/XMLSchema#string">
                            <AttributeValue>$subject</AttributeValue>
                        </Attribute>  
                    </Subject>
                    <Resource>
                        <Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id" DataType="http://www.w3.org/2001/XMLSchema#string">
                            <AttributeValue>$resource</AttributeValue>
                        </Attribute>
                    </Resource> 
                    <Action>
                        <Attribute AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id" DataType="http://www.w3.org/2001/XMLSchema#string">
                            <AttributeValue>$action</AttributeValue>
                        </Attribute> 
                   </Action>                  <Environment/>
                </Request> 
                """
                
                val response = client.post(XACML_PDP) {
                    header("domain", XACML_DOMAIN)
                    setBody(req)
                }

                val respBody = response.bodyAsText()

                if (response.status.value != 200){
                    printLog("There was an error with XACML\n")
                    return false
                }

                val regex = Regex("<Decision>(.*?)</Decision>")
                val match = regex.find(respBody)
                val decision = match?.groupValues?.get(1)

                if (decision != null && decision == "Permit"){
                    return true
                } else {
                    printLog("Deny/Not Applicable\n")
                }

                return false
                   
            }

            fun storeHist(method: String, resource: String, requester: String, accesstoken: String) {
                val currentDateTime = LocalDateTime.now()
                val formattedDateTime = currentDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)

                val uniqueId = UUID.randomUUID().toString()

                ATHIST.initialize()
                ATHIST.setValue(uniqueId, formattedDateTime, method, resource, requester, accesstoken)
                ATHIST.shutdown()
            }
        }

