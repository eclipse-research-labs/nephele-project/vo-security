package id.walt.services.did.composers

import id.walt.model.DID_CONTEXT_URL
import id.walt.model.VerificationMethod
import id.walt.model.did.DidFabric
import id.walt.services.did.composers.models.DocumentComposerBaseParameter
import id.walt.services.did.composers.models.DocumentComposerKeyJwkParameter

class DidFabricV1DocumentComposer : DidDocumentComposerBase<DidFabric>() {
    override fun make(parameter: DocumentComposerBaseParameter): DidFabric =
        (parameter as? DocumentComposerKeyJwkParameter)?.let {
            val kid = it.didUrl.did + "#" + it.key.keyId
            val verificationMethods = buildVerificationMethods(it.key, kid, it.didUrl.did, it.jwk)
            val keyRef = listOf(VerificationMethod.Reference(kid))
            DidFabric(
                listOf(DID_CONTEXT_URL), // TODO Context not working "https://ebsi.org/ns/did/v1"
                it.didUrl.did, verificationMethods, keyRef, keyRef
            )
        } ?: throw IllegalArgumentException("Couldn't parse fabric-v1 document composer parameter")
}
