package id.walt.cli

import com.github.ajalt.clikt.core.CliktCommand
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import java.net.URI
import id.walt.services.oidc.OIDC4CIService
import java.security.SecureRandom
import com.google.gson.Gson
import com.nimbusds.jwt.JWTClaimsSet
import id.walt.services.did.DidService
import id.walt.services.jwt.JwtService
import id.walt.model.oidc.*
import id.walt.services.key.KeyService
import java.util.*
import id.walt.services.keystore.KeyType
import id.walt.common.SqlDbManager
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObjectBuilder
import id.walt.credentials.w3c.JsonConverter
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject


import org.bitcoinj.core.Base58




/* SSIKIT Holder */
class TestIssuer :
        CliktCommand(
                name = "testIssuer",
                help =
                        """Ejemplo de conexión con Relaying party.
         """
        ) {

        // endpoints
        val ENDPOINT_LIST_CREDENTIALS = "http://localhost:8099/list"
        val ENDPOINT_AUTH = "http://localhost:8099/auth"
        val ENDPOINT_CODE = "http://localhost:8099/code"
        val ENDPOINT_TOKEN = "http://localhost:8099/token"
        val ENDPOINT_CREDENTIAL = "http://localhost:8099/credential"

        // Salida mas legible
        val verde = "\u001B[32m"
        val rojo = "\u001B[31m"
        val reset = "\u001B[0m"

        // DID
        val ISSUER_DID = "did:ebsi:zq3kRZe5vsTWHchrUVPbX3R"

        // Servicios
        private val keyService = KeyService.getService()
        val keyID = "9339eefce5f54e5b88d1d1a56e55228e"

        /* 
        init {
            SqlDbManager.start()
        }
        */
    
        override fun run() = runBlocking {

            val client =
                    HttpClient() {
                        install(ContentNegotiation) { json() }
                        expectSuccess = false
                    }
                        
             // Obtención del listado de credenciales disponibles
             
            val credentialType = get_OIDC_discovery_document(client)
            
            // Obtención de código de autenticación

            val uri = push_OIDC_auth_request(client)

            val state: String = generarStateAleatorio()

            println("uri: "+uri)
            println("state: "+state)

            // Obtención del JWT pre_auth 

            val texto = get_JWT_and_STATE(client, state, uri)
            
            val partes = texto.split(":")
            val jwt = partes[0]
            val stateResponse = partes[1]
    
            println("JWT:"+ jwt)
            println("STATE:"+ stateResponse)

            if(state != stateResponse){
                println("")
                println("[!] States do not match! The Issuer response is not valid")
            } 
            else{

                println("")
                println("[+] States match! The Issuer response is valid")

                // Obtener access token access Token

                val token = get_Access_Token(client, jwt)
                println(token)

                // Obtener la credencial

                val credential = getCredential(client, token, credentialType)    
                println(credential)   
            }



            client.close()

        }

    suspend fun getCredential(client: HttpClient, token: String, credentialType: String): String {
        println("")
        println(verde+"[+] GET credential"+reset)
        println("")

        val gson = Gson()
        val jsonObject = gson.fromJson(token, Map::class.java)
        var access_token = jsonObject["access_token"] as String
        println("access token: "+access_token)
        var valor_nonce = jsonObject["c_nonce"] as String
        println("c_nonce: "+valor_nonce)
        val template = credentialType
        val attributes= buildJsonObject {
            put("id", JsonConverter.toJsonElement("vo1"))
            put("type", JsonConverter.toJsonElement("VO"))
            put("description", JsonConverter.toJsonElement("VO deployed in UC#3 scenario"))
        }
        val attributesString = Json.encodeToString(attributes)

        val nonce_signed = generateDidProof(ISSUER_DID,valor_nonce)
        //val key = getPublicKey(keyID)

        val response = client.get(ENDPOINT_CREDENTIAL) {
            url {
                parameters.append("nonce_signed", nonce_signed.toString())
                parameters.append("template", template)
                parameters.append("token", access_token)
                parameters.append("attributes", attributesString)
                //parameters.append("publicKey", key)

            }
        }


        //println("public key: "+key)
        println("acces_token: "+access_token)
        println("nonce_signed: "+nonce_signed)
        println("nonce: "+valor_nonce)

        val credential: String = response.bodyAsText()
        return credential
    }

    suspend fun get_Access_Token(client: HttpClient, jwt: String): String {
        println("")
        println(verde+"[+] GET access Token"+reset)
        println("")

        var response = client.get(ENDPOINT_TOKEN) {
            url {
                parameters.append("jwt", jwt)
            }
        }

        val token: String = response.bodyAsText()
        return token
    }

    suspend fun push_OIDC_auth_request(client: HttpClient): String{
        println("")
        println(verde+"[+] PUSH OIDC auth request"+reset)
        println("")

        var response = client.get(ENDPOINT_AUTH) {

        }

        val uri: String = response.bodyAsText()

        return uri

    }

    suspend fun get_JWT_and_STATE(client: HttpClient, state: String, uri: String): String{

        println("")
        println(verde+"[+] GET JWT and STATE"+reset)
        println("")


        var response = client.get(ENDPOINT_CODE) {
            url {
                parameters.append("uri", uri)
                parameters.append("state", state)
            }
        }

        val texto: String = response.bodyAsText()
        return texto

    }

    suspend fun get_OIDC_discovery_document(client: HttpClient): String{
        println("")
        println(verde+"[+] GET OIDC discovery document"+reset)
        println("")

        var response = client.post("$ENDPOINT_LIST_CREDENTIALS") {
            setBody("")
        }

        val locationHeader = response.headers[HttpHeaders.Location]
        var body: String = response.bodyAsText()
        
        val issuer = OIDC4CIService.getWithProviderMetadata(OIDCProvider(ENDPOINT_LIST_CREDENTIALS, ENDPOINT_LIST_CREDENTIALS))

        println("---")
        var x = 0
        OIDC4CIService.getSupportedCredentials(issuer).forEach { supported_cred ->
            println("Issuable credentials:")
            println("${x}- ${supported_cred.key}")
            println("---")
            x++
        }

        print("Select credential type to be created:")
        //val credentialTypes = readLine()
        val credentialTypes="NepheleId"
        println("")
        println(verde+"[+] Credential selected -> ${credentialTypes}"+reset)
        println("")

        return credentialTypes.toString()
    }


    fun generateDidProof(did: String, nonce: String): JwtProof {
        val didObj = DidService.load(did)
        val vm = (didObj.authentication ?: didObj.assertionMethod ?: didObj.verificationMethod)?.firstOrNull()?.id ?: did
        return JwtProof(
            jwt = JwtService.getService().sign(
                vm,
                JWTClaimsSet.Builder()
                    .issuer(did) // Utiliza el DID como emisor en lugar de issuer.client_id o did
                    .issueTime(Date())
                    .claim("nonce", nonce)
                    .build().toString()
            ),
        )
    }


    fun generarStateAleatorio(): String {
        val secureRandom = SecureRandom()
        val bytes = ByteArray(32)
        secureRandom.nextBytes(bytes)

        val base64String = Base64.getUrlEncoder().withoutPadding().encodeToString(bytes)
    
        val state = base64String.substring(0, 16) + "-" + base64String.substring(16)
        
        return state
    }


    /* 
    fun getPublicKey(keyId: String): String {
        var publicKey  = ""
        SqlDbManager.getConnection().use { connection ->
            connection.prepareStatement("select * from lt_key where name = ?").use { statement ->
                statement.setString(1, keyId)
                statement.executeQuery().use { result ->
                    if (result.next()) {
                        publicKey = result.getString("pub")
                    }
                }
                connection.commit()
            }
        }
        return publicKey
    }
    */



}