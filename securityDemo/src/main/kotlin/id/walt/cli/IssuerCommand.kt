package id.walt.cli

import com.github.ajalt.clikt.core.CliktCommand
import io.ktor.server.application.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.net.URI
import java.security.SecureRandom
import java.util.UUID
import java.time.Instant
import java.util.Base64
import com.auth0.jwt.JWT
import com.auth0.jwt.interfaces.DecodedJWT
import id.walt.model.credential.status.CredentialStatus
import id.walt.sdjwt.DecoyMode
import id.walt.sdjwt.SDMap
import id.walt.signatory.Ecosystem
import id.walt.signatory.ProofConfig
import id.walt.signatory.ProofType
import id.walt.signatory.Signatory
import io.ktor.server.netty.*
import com.nimbusds.jwt.SignedJWT 
import id.walt.sdjwt.JwtVerificationResult
import id.walt.crypto.*
import id.walt.services.key.KeyService
import com.nimbusds.jose.crypto.*
import org.bouncycastle.asn1.ASN1Sequence
import id.walt.services.jwt.JwtService
import org.bouncycastle.asn1.ASN1BitString;
import java.util.*
import com.nimbusds.jwt.JWTClaimsSet
import kotlinx.coroutines.runBlocking
import id.walt.signatory.dataproviders.CLIDataProvider
import io.ktor.http.HttpStatusCode
import id.walt.common.createDid
import id.walt.common.genKeys
import id.walt.common.printLog
import id.walt.services.did.DidService


/* SSIKIT issuer */
class IssuerCommand :
        CliktCommand(
                name = "issuer",
                help =
                        """
                        Servidor HTTP para el Relying Party (Issuer)
                """
        ) {
    
            data class TokenRecord(val requestUri: String, val expirationTime: Instant)
            val tokenRegistry = mutableMapOf<String, TokenRecord>()
            val nonceRegistry = mutableMapOf<String, String>()
            val JWTRegistry = mutableSetOf<String>()
            // KEY
            val DEFAULT_ALGORITHM = "Secp256k1"
            // DID
            val DEFAULT_DID_METHOD = "fabric"
            lateinit var DID: String
            // More legible output
            val green = "\u001B[32m"
            val red = "\u001B[31m"
            val reset = "\u001B[0m"

            override fun run() {
                // Create DID
                val keyAlias = genKeys(DEFAULT_ALGORITHM)
                printLog(keyAlias)
                createDid(DEFAULT_DID_METHOD, keyAlias)
                DID = DidService.getDid()

                embeddedServer(Netty, host = "0.0.0.0", port = 8080) {
                            routing {
                                    get("/auth"){
                                        println("")
                                        println(green + "[+] PUSH OIDC auth request" + reset)
                                        println("")
    
                                        /*
                                            En este punto debería darse el proceso de autenticación del usuario.
                                        */
    
                                        val uriAleatorio = generateRandomURI()
                                        println("Response: " + uriAleatorio)
                                        call.respond(HttpStatusCode.OK, uriAleatorio)
                                    }
    
                                    get("/code"){
                                        println("")
                                        println(green + "[+] GET JWT pre_auth" + reset)
                                        println("")
                                        var uri = call.parameters["uri"]
                                        var state = call.parameters["state"]
                                        if ( uri == null || uri.isEmpty()){
                                            println()
                                            println(red + "[!] ERROR: you have to specify a URI." + reset)  
                                            call.respond(HttpStatusCode.BadRequest, "ERROR: you have to specify a URI.")
                                        }
                                        else if ( state == null || state.isEmpty()){
                                            println()
                                            println(red + "ERROR: you have to specify a state." + reset)  
                                            call.respond(HttpStatusCode.BadRequest, "ERROR: you have to specify a state.")
                                        }
                                        else{
                                            val bool = verifyToken(uri)
                                            if (!bool) {
                                                println()
                                                println(red + "ERROR: invalid or expired token." + reset)  
                                                call.respond(HttpStatusCode.Unauthorized, "ERROR: invalid or expired token.")
                                            }
                                            else {
                                                val jwt = buildPreJWT(uri)
                                                if (!JWTRegistry.contains(uri)) {
                                                    JWTRegistry.add(uri)
                                                }
                                                println("Response: ")
                                                println("JWT: " + jwt)
                                                println("state: " + state)
                                                call.respond(HttpStatusCode.OK, jwt + ":" + state)
                                            }
    
                                        }
                                    }
    
                                    get("/token"){
                                        println("")
                                        println(green + "[+] GET access Token" + reset)
                                        println("")
                                        var jwt = call.parameters["jwt"]
                                        if ( jwt == null || jwt.isEmpty()){
                                            println()
                                            println(red + "ERROR: Required parementers: JWT." + reset)  
                                            call.respond(HttpStatusCode.BadRequest, "ERROR: Required parementers: JWT.")
                                        }
                                        else{
                                            val sub = verifyAndExtractUUID(jwt)
                                            if(sub != null ){
                                                if (!JWTRegistry.contains("urn:ietf:params:oauth:request_uri:" + sub)){
                                                    println()
                                                    println(red + "[!] JWT pre_auth token is already used" + reset)
                                                    call.respond(HttpStatusCode.Unauthorized, "ERROR: JWT pre_auth token is already used")
                                                }
                                                else{
                                                    JWTRegistry.remove("urn:ietf:params:oauth:request_uri:" + sub)
                                                    val bool = verifyToken("urn:ietf:params:oauth:request_uri:" + sub)
                                                    if (!bool) call.respond(HttpStatusCode.Unauthorized, "ERROR: invalid or expired token.")
                                                    else{
                                                        val nonce = generateRandomValue()
                                                        nonceRegistry[sub] = nonce;
                                                        val token = generateAccessToken(sub,generateRandomValue(),nonce,buildJWT(sub))
                                                        println(token)
                                                        call.respond(HttpStatusCode.OK, token)
                                                    }
                                                }
                                            }
    
                                        }
    
                                    }
    
                                    get("/credential"){
                                        println("")
                                        println(green + "[+] Get credential from issuer." + reset)
                                        println("")
                                        var nonce_signed = call.parameters["nonce_signed"]
                                        var template = call.parameters["template"]
                                        var token = call.parameters["token"]
                                        var attributes = call.parameters["attributes"]
                                        //var publicKey = call.parameters["publicKey"]
    
                                        
                                        if ( template == null ||  token == null || nonce_signed == null || token.isEmpty() || nonce_signed.isEmpty() || template.isEmpty() ){
                                            call.respond(HttpStatusCode.BadRequest, "ERROR: You have to specific all the required parameters")
                                        }
                                        else{
    
                                            // Verify that the access token is not expired
                                            if(verifyToken("urn:ietf:params:oauth:request_uri:" + token)){
                                                println("template: " + template)
                                                println("token: " + token)
                                                println("nonce_signed: " + nonce_signed)
        
                                                
                                                val jwtRegex = Regex("jwt=([^)]+)")
                                                val matchResult = jwtRegex.find(nonce_signed)
                                                if (matchResult != null) {
                                                    val jwt = matchResult.groupValues[1]
                                                    val decodedJWT: DecodedJWT = JWT.decode(jwt)
                                                    val issuer = decodedJWT.issuer
                                                    val iat = decodedJWT.issuedAt
                                                    val nonce = decodedJWT.getClaim("nonce").asString()
                                                    
        
                                                    println()
                                                    println("Issuer: $issuer")
                                                    println("IAT (Issued At): $iat")
                                                    println("Nonce: $nonce")
                                                    println()
                                                    
                                                    val checkCorrectNonce = nonceRegistry[token]
        
                                                    val v = JwtService.getService().verify(jwt)
                                                    println(v)
                                                    
                                                    // Check that the 'nonce' value is signed correctly
                                                    if(v.verified && (checkCorrectNonce == nonce)){
                                                        println("Nonce is signed correctly")
                                                        println("Creating credential...")
                                                        val ldSignatureType: LdSignatureType? = null
                                                        val issuerVerificationMethod: String? = null
                                                        val credentialTypes: CredentialStatus.Types? = null
                                                        val selectiveDisclosurePaths: List<String>? = null
                                                        println("Subject: " + issuer)
                                                        println("Issuer: " + DID)
                                                        println()
                                                        val credential = createCredential(DID,issuer,template, issuerVerificationMethod, ProofType.LD_PROOF, "assertionMethod", ldSignatureType, Ecosystem.DEFAULT , credentialTypes, DecoyMode.NONE, 0, selectiveDisclosurePaths, attributes)
                                                        println(credential)
                                                        call.respond(HttpStatusCode.OK, credential)
                                                    }
                                                    else{
                                                        println(red + "[!] ERROR: Couldn't verify signature." + reset)
                                                        call.respond(HttpStatusCode.Unauthorized, "Couldn't verify signature")
        
                                                    }    
                                                }
                                                else{
                                                    println("Incorrect JWT format.")
                                                    call.respond(HttpStatusCode.BadRequest, "Incorrect JWT format")
                                                }  
                                            }
                                            else
                                            {
                                                println("Incorrect JWT format.")
                                                call.respond(HttpStatusCode.BadRequest, "Incorrect JWT format")
                                            }
                                        }
                                    }
                                
                            }
                }.start(wait = true)                
            }

            fun generateRandomURI(): String {
                val uuid = UUID.randomUUID()
                val uri = "urn:ietf:params:oauth:request_uri:${uuid.toString()}"
                val expirationTime = Instant.now().plusSeconds(60)
                tokenRegistry[uri] = TokenRecord(uri, expirationTime)
                return uri
            }
            
            fun verifyToken(uri: String): Boolean {
                val tokenRecord = tokenRegistry[uri]
                
                if (tokenRecord != null) {
                    val ahora = Instant.now()
                    if (tokenRecord.expirationTime.isAfter(ahora)) {
                        return true 
                    } else {
                        tokenRegistry.remove(uri)
                    }
                }
                
                return false // The token is not valid or has expired
            }

            fun buildPreJWT(uri: String): String {
                val uuid = uri.substringAfterLast(":")
                return JwtService.getService().sign(DID, JWTClaimsSet.Builder()
                    .claim("sub", uuid)
                    .claim("pre-authorized", "false")
                    .build().toString()
                )
            }

            fun buildJWT(uri: String): String {
                val uuid = uri.substringAfterLast(":")
                return JwtService.getService().sign(DID, JWTClaimsSet.Builder()
                    .claim("sub", uuid)
                    .build().toString()
                )
            }

            fun generateAccessToken(usuario: String, refreshToken: String, cNonce: String, idToken: String): String {
                val accessToken = """
                    {
                        "access_token": "$usuario",
                        "refresh_token": "$refreshToken",
                        "c_nonce": "$cNonce",
                        "id_token": "$idToken",
                        "token_type": "Bearer",
                        "expires_in": 60
                    }
                """.trimIndent()
            
                return accessToken
            }

            fun verifyAndExtractUUID(jwt: String): String? {
                val verificationResult = JwtService.getService().verify(jwt) 
                if (verificationResult.verified) {
                    val claims = JwtService.getService().parseClaims(jwt) 
                    val sub = claims?.get("sub") 
                    if (sub is String) {
                        return sub 
                    }
                }
                return null 
            }

            fun createCredential(issuerDid: String, subjectDid: String, template: String, issuerVerificationMethod: String?, proofType: ProofType, proofPurpose: String, ldSignatureType: LdSignatureType?, ecosystem: Ecosystem , statusType: CredentialStatus.Types?, decoyMode: DecoyMode, numDecoys: Int, selectiveDisclosurePaths: List<String>?, attributes: String?): String {
                val signatory = Signatory.getService()
                val selectiveDisclosure = selectiveDisclosurePaths?.let { SDMap.generateSDMap(it, decoyMode, numDecoys) }
        
                val vcStr: String = runCatching {
                    signatory.issue(
                        template, ProofConfig(
                            issuerDid = issuerDid,
                            subjectDid = subjectDid,
                            issuerVerificationMethod = issuerVerificationMethod,
                            proofType = proofType,
                            proofPurpose = proofPurpose,
                            ldSignatureType = ldSignatureType,
                            ecosystem = ecosystem,
                            statusType = statusType,
                            creator = issuerDid,
                            selectiveDisclosure = selectiveDisclosure
                        ), attributes, CLIDataProvider
                    )
                }.getOrElse { err ->
                    when (err) {
                        is IllegalArgumentException -> echo("Illegal argument: ${err.message}")
                        else -> echo("Error: ${err.message}")
                    }
                    return "Error creating the VC"
                }
        
                return vcStr
            }

            fun generateRandomValue(): String {
                val secureRandom = SecureRandom()
                val bytes = ByteArray(32)
                secureRandom.nextBytes(bytes)
        
                val base64String = Base64.getUrlEncoder().withoutPadding().encodeToString(bytes)
            
                val state = base64String.substring(0, 16) + "-" + base64String.substring(16)
                
                return state
            }
}
