package id.walt.cli

import com.github.ajalt.clikt.core.CliktCommand
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.request.post
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import id.walt.services.oidc.OIDC4VPService
import java.net.URI
import id.walt.services.oidc.OIDCUtils
import id.walt.common.KlaxonWithConverters
import id.walt.common.prettyPrint
import id.walt.custodian.Custodian
import id.walt.credentials.w3c.toVerifiablePresentation
import com.nimbusds.oauth2.sdk.ResponseMode
import id.walt.services.oidc.CompatibilityMode
import id.walt.credentials.w3c.PresentableCredential
import java.util.*
import id.walt.services.did.DidService
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObjectBuilder
import id.walt.credentials.w3c.JsonConverter
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import com.nimbusds.oauth2.sdk.http.HTTPRequest
import id.walt.common.printLog





/** CLI Command to run the walt.id SSI KIT as RESTful service. */
class DemoVO :
        CliktCommand(
                name = "vo",
                help =
                        """
         """
        ) {
    override fun run() = runBlocking {

        // APIs
        val HOLDER_API_URL = "http://oidc4vp-holder:8080/auth"
        val ORIONLD_API = "http://orionld:1026/version"
        val DEFAULT_VC_TEMPLATE = System.getenv("default_vc_template").toString()

        val logs: Boolean = System.getenv("logs").toBoolean()

        val client =
                HttpClient() {
                    install(ContentNegotiation) { json() }
                    expectSuccess = false
                }

        
        // Request VC if there is no VC Stored
        val response = client.get("http://oidc4vp-holder:8080/isVCStored")
        if (response.status.value != 200){
            printLog("VC not stored, requesting VC...")
            requestVC(client, DEFAULT_VC_TEMPLATE)
        } else {
            printLog("There is already a VC stored")
        }

        // Create JSON
        val json: JsonObject = buildJsonObject {
            put("device", JsonConverter.toJsonElement("http://oidc4vp-proxy:8080"))
            put("method", JsonConverter.toJsonElement("GET"))
            //put("resource", JsonConverter.toJsonElement("/ngsi-ld/v1/entities/urn:a.*"))
            put("resource", JsonConverter.toJsonElement("/ngsi-ld/v1/entities/urn:a.*"))
            put("requester", JsonConverter.toJsonElement("192.168.1.1"))
        }

        val jsonString = Json.encodeToString(json)
        
        if (logs){
            printLog("JSON: $jsonString\n")
        }
        

        // Repeat the execution for test purposes 
        while (true) {
            // the cVO sends the JSON to its Holder
            val response = client.post("$HOLDER_API_URL") {
                contentType(ContentType.Application.Json)
                setBody(jsonString)
            }

            // Error handling
            val statusCode = response.status.value
            if (statusCode != 200) {
                if (statusCode == 401){
                    printLog("UNAUTHORIZED: ${response.bodyAsText()}")
                    Thread.sleep(7000)
                    continue
                } else if(statusCode == 404) {
                    printLog("ERROR: $response")
                    val vcId: String = response.bodyAsText()
                    printLog("vcId: $vcId")
                    requestVC(client, vcId)
                    continue
                }
                //System.exit(1)
            }

            if (logs){
                printLog("$response\n")
            }
            

            // Get Access Token from the response
            var accessToken: String = response.bodyAsText()
            accessToken = accessToken.replace("\n", "")

            printLog("1. RECEIVED -> Access Token: $accessToken\n")

            // First request
            printLog("cVO wants to consume data from http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:a1111...")
            val res = client.get("http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a1111"){
                header("x-auth-token", accessToken)
            }
            val version = res.bodyAsText()
            printLog("Access Token is valid to consume resource http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a1111")
            printLog("Obtained Data: $version")

            // Second request
            printLog("cVO wants to consume data from http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:a2222...")
            val res2 = client.get("http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:a2222"){
                header("x-auth-token", accessToken)
            }
            val version2 = res2.bodyAsText()
            printLog("Access Token is valid to consume resource http://oidc4vp-proxy:8080/ngsi-ld/v1/entities/urn:a2222")
            printLog("Obtained Data: $version2")

            // Third request
            printLog("cVO wants to consume data from http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:b1111...")
            try{
                val res3 = client.get("http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:b1111"){
                    header("x-auth-token", accessToken)
                }
                val version3 = res3
                printLog("Obtained Data: $version3")
            } catch (e: Exception) {
                printLog("Unauthorized: Access Token is not valid to access resource http://oidc4vp-proxy:9090/ngsi-ld/v1/entities/urn:b1111\n")
            }

            Thread.sleep(10000)
        }
    }

    suspend fun requestVC(client: HttpClient, vcTemplate: String) {
        printLog("Verifiable Credential not stored in Holder, starting process to obtain Verifiable Credential\n")
        val attributes: JsonObject = buildJsonObject {
            put("uuid", JsonConverter.toJsonElement("vo1"))
            put("type", JsonConverter.toJsonElement("VO"))
            put("description", JsonConverter.toJsonElement("VO deployed in UC#3 Energy Management scenario."))
            put("vcTemplate", JsonConverter.toJsonElement(vcTemplate))
        }
        val attributesString = Json.encodeToString(attributes)
        val res = client.post("http://oidc4vp-holder:8080/obtainCredential"){
            contentType(ContentType.Application.Json)
            setBody(attributesString)
        }
        if (res.status.value == 200){
            printLog("Verifiable Credential Obtained!")
        } else {
            printLog("There was an error obtaining the Verifiable Credential")
        }
    }
}
