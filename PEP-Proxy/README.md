# License

PEP-Proxy Project source code files are made avaialable under the Apache License, Version 2.0 (Apache-2.0), located into the LICENSE file.

# What is a PEP-Proxy

PEP-Proxy is the component responsible for receiving the queries, from a client entity (applications, services, users, devices...), aimed to access to a resource, they are accompanied by the corresponding authorisation token (Capability Token (CT)) issued by the Capability Manager and forwards requests to the corresponding endpoint of a target component (for example a repository/broker) and the responses back to the requester.

This component is developed in Python and is an element of the DCapBAC technology, as can be seen in the following image:

![DCapBAC-Architecture](./img/DCapBAC-Architecture.png)

Remembering DCapBAC technology, where access control process is decoupled in two phases:

- 1st operation to receive authorisation. An authorisation token is issued.
- 2nd operation access to the resource previously validating the authorisation token.

PEP-Proxy covers the second one.

## Project details

This project contains:

- Main source files & folders.

    - [PEP-Proxy.py](./PEP-Proxy.py): Main file of the component, contains the functionality and allows the execution of the HTTP server.
    - [config.cfg](./config.cfg): Contain the configuration of the component.
    - [UtilsPEP.py](./UtilsPEP.py): Handle several requests receives by PEP-Proxy sending it to the corresponding API functionality.
    - [API](./API/): Contain one file per specific API. Each one has the functionality to validate and cypher the request body.

- Folder to ssl (offer HTTS).

    - [certs](./certs/): Contains the certificates needed to ssl.

- Folder to verify the authorisation token.
    - [pyCapabilitylib](./pyCapabilitylib/): To verifity the authorisation token.

- Files to deploy the component.

    - [requirements.txt](./requirements.txt): Contain the auxiliar python modules needed by the application. Dockerfile uses it. 
    - [Dockerfile](./Dockerfile): Contain the actions and commands to create the Docker image needed to run the component.
    - [docker-compose.yml](./docker-compose.yml): To deploy the component.

- Files & folder to cypher.
    - [conf_files](./conf_files/): Contain key files.
    - [cpabe_cipher.jar](./cpabe_cipher.jar): It is a Java application to cypher text with CP_ABE.
    - [cipher_configuration.json](./cipher_configuration.json): It is the configuration file of [cpabe_cipher.jar](./cpabe_cipher.jar).

- Folder to test.

    - [test](./test/): Contain a forder with python client examples to NGSI-LD. 

- File to monitor.

    - [monitor_PEP-Proxy.sh](./monitor_PEP-Proxy.sh): To test if the component is running or not.

- File to clean docker logs.

    - [deleteLogs.sh](./deleteLogs.sh): To clean logs of the component (registered and used by docker).
    **NOTE:** This script remove the logs of all dockerized services present in the machine not only the logs of Capability Manager. 

# PEP-Proxy functionality in DCapBAC

This component receives queries aimed to access to a resource, queries contain a header with a Capability Token. The PEP-Proxy validates this token, and in case the evaluation is positive, it forwards requests to the specific endpoint’s API. When an access resource request is received by PEP-Proxy:

- recovers the x-auth-token header (Capability Token).
- validate Capability Token.
- If Capability Token validation is successful, PEP-Proxy recovers the request body, if it exists, and cypher it if it is necessary.

- Finally, PEP-Proxy forwards the message and sends responses back to the requester.

![security-authorisation-PEP](./img/plantuml/security-authorisation-PEP.png)


# PEP-Proxy - Resume of Features/Limitations

- Supported actions/methods: GET, POST, PATCH, PUT, DELETE. To increase supported API actions/methods or paths you must review PEP-Proxy files. It's not be a problem if you don't need use cipher methods. If it may be necessary to support new actions/methods or paths that use cipher, see the "Proxy PEP - supporting new actions/path with cypher" section.

- Supported APIs: Supports any API including standards as NGSIv1, NGSIv2, NGSI-LD. To know how implement a new API, see "Proxy PEP - supporting new API" section of this document.

- Cipher:
    - Supported cipher method: cp-abe. If it may be necessary to support new cipher methods, see the "Proxy PEP - supporting new cypher" section of this document. 
    - Supported APIs uses cipher method (cp-abe): NGSIv2, NGSILDv1
    - No supported cipher for "id", "type" or "@context" first-level keys.

- If update relativePathAttributeEncriptation param, PEP-Proxy code must be update, see "Proxy PEP - updating relativePathAttributeEncriptation param" section

- To cypher all entity attributes we can do it if defining one condition that all attributes satisfy, for example, all attributes include a key named "type" or "value", before do it see also "Proxy PEP - updating relativePathAttributeEncriptation param" section: 

    ```sh
    relativePathAttributeEncriptation=[[["type",""]]]
    ```

- No supported:
    - PUT methods doesn't support cyphered.

- PEP-Proxy errors or validations return a simulated fixed response from API. At the moment some headers are missed comparing with the original API response (Connection, Content-Length), because they create errors.

# How to deploy/test

## Prerequisites to deploy PEP-Proxy

To run this component is neccessary to install the docker-compose tool.

https://docs.docker.com/compose/install/

Launch next components to obtain the authorisation token:

- Keyrock component running and configured with the users, applications, organizacions... required to configure the XACML access control policies.
- PDP component running. Do not forget that PDP requires XACML access control policies to be defined and in this sense the PAP component can be used to facilitate this task. Remember that the PAP and PDP components are elements of the XACML framework and in a typical scenario two components should be running.
- Capability Manager. Required to create the capability Token required by the PEP-Proxy.

Launch next components to access the resource:

- Target component is running (broker, application, ...) to allow the forwarding of the requests from the PEP-Proxy.

# Configuration config.cfg file

Before launching the PEP-Proxy, it's necessary to review the [config.cfg](./config.cfg) file, having:

```sh
vi config.cfg
```

- Params to PEP-Proxy's endpoint.

    - `pep_host`: No change admittable (always 0.0.0.0)
    - `pep_port`: If change this param, it's necessary to review Dockerfile and docker-compose.yml files. By default 1027.

- Params to the target's endpoint.

    - `allApiHeaders`: Define the admitted headers to send to target API. If a header of the request receives by PEP-Proxy is not included in this param, the final API request will not consider it. This param store all the headers of each API supported by PEP-Proxy. It's necessary to define an element for each API supported. **IMPORTANT:** It's necessary to use lower case.
    - `chunk_size`: To read the response received after the API request. Default 1024

- Params to cipher attributes.

    - `allSeparatorPathAttributeEncriptation`: Specify the separator used by relativePathAttributeEncriptation param to build a relative path into the attributes. This relative path is necessary to determine if an attribute requires cypher or no. Use a pattern never used by attributes or keywords. This param store a separator of each API supported by PEP-Proxy. It's necessary to define an element for each API supported.
    - `relativePathAttributeEncriptation`: This parameter is a tridimensional array. To understand it, we are going to use the next examples:

        **ONLY ONE CONDITION**. In this case, the system searches into each the attribute first a key named "metadata", and after, into it, a key named "cpabe-policy". If it is successful, the attribute will cypher.

        - Example (NGSIv2).

            ```sh
            relativePathAttributeEncriptation=[[["metadata/cpabe-policy",""]]]
            ```

        **NOTE:** If the second element of the array is defined, the system also will verify if the value is the same as the relative path value. If it is also successful, the attribute will cypher.

        - Example (NGSIv1), the analog of previous NGSIv2 example:

            ```sh
            relativePathAttributeEncriptation=[[["metadatas/name","cpabe-policy"]]]
            ```
            
        **TWO CONDITIONS AND MORE (AND)**. In this case, the second condition must also be satisfied. It is an "and" condition. If both conditions are successful, the attribute will cypher.

        - Example (NGSIv1).

            ```sh
            relativePathAttributeEncriptation=[[["metadatas/name","cpabe-policy"],["metadatas/type","policy"]]]
            ```

        **TWO CONDITIONS AND MORE (OR).**
        In this case, if one of the conditions is successful, the attribute will cypher. It is an "or" condition.

        - Example (NGSIv1):

            ```sh
            relativePathAttributeEncriptation=[[["metadatas/name","cpabe-policy"]],["metadatas/name","other-policy"]]]
            ```     

        **NOTE:** Mixed cases are supported.

        **IMPORTANT:** This param is case sensitive.

        **IMPORTANT:** Before update this param see "Proxy PEP - updating relativePathAttributeEncriptation param" section.

    - `noEncryptedKeys`: It is a list of first-level attribute keys that never will be cyphered. **IMPORTANT:** It's necessary to use lower case. Default ["id","type","@context"].

- Params to log KPIs info

    - `logginKPI`=Admitted values: `Y`,`N`. With `Y` defined, PEP-Proxy will include more information in the logs to report the time spent by each component subtask, useful for KPI latency measurement. In this sense, the component can measure in milliseconds):
        - `Total(ms) Validacion process`: Dedicated time to validate the signed Capability Token including the Blockchain access.
        - `Total(ms) Encrypt process`: Dedicated time to cipher the body with CP-ABE.

## Configuration .env/docker-compose.yml files

Review the contain of [.env](./.env) and [docker-compose.yml](./docker-compose.yml) to configure the instance.

### Params related to PEP-Proxy exposed endpoint

- `PEP_PROTOCOL`: Admitted values: `https`,`http`. Default `https`. If "pep_protocol"="https", certificated files must be generated. Copy generated certificates files (fullchain.pem and privkey.pem) to [certs](./certs/) folder. You have to respect these file names or update volumes section of pepproxy service in docker-compose file.

- `PEP_EXPOSED_PORT`: Define the exposed port for external accesses to the PEP-Proxy API. By default the `1027` port is configured.


### Params to the target's endpoint.

- `PEP_TARGET_PROTOCOL`: Target component protocol. Admitted values: `http`,`https`.
- `PEP_TARGET_HOST`: Target component host.
- `PEP_TARGET_PORT`: Target component port.
- `PEP_TARGET_API`: Target component API type. Admitted values: `NGSIv1`,`NGSIv2`,`NGSILDv1`,`GenericAPI`.

### Params to the blockchain's endpoint.

- `PEP_BLOCKCHAIN_USEVALIDATION`=Validate Capability token using blockchain: Admitted values: `0`: No use; `1`:Use.
- `PEP_BLOCKCHAIN_API`=NativeAPI #Optional: Default value: `NativeAPI` Admittable values: `NativeAPI`, `NGSIv2`, `NGSI-LD`.
- `PEP_BLOCKCHAIN_PROTOCOL`=BlockChain protocol. Admitted values: `http`,`https`.
- `PEP_BLOCKCHAIN_HOST`=Blockchain public IP.
- `PEP_BLOCKCHAIN_PORT`=Blockchain port.
- `PEP_BLOCKCHAIN_GET_TOKEN`=Reource to obtain capabilityTokens from Blockchain # Optional : Default value : `/token`.

### Params to the PEP's endpoint in PDP component.

- `PEP_ENDPOINT`=PEP-Proxy Public address ex: https://<PEP-IP>:<PEP-PORT>>. HOST NO admitted: `0.0.0.0`, `localhost`, `127.0.0.1`

### Param to enable CORS.

- `PEPPROXY_CORS_ENABLED`=Admitted values: `1` - if you need to add CORS headers or `0` - if you don't need to add CORS headers

### Hiden params to sign the capability token. 

There are specific parameters related to verify a CT. They are not exposed to [docker-compose.yml](docker-compose.yml) file to avoid bad configuration. They only must be changed by responsible persons in excepcionals scenarios.

- `pyCapabilityLib_folderpath=pyCapabilityLib/`. Define the path that contain all the logic to verify a signed CT. No changes admitted.
- `pyCapabilityLib_signMethod=certsFile`. Define the method to verify the Capability Token. Admitted values:
    - `certsFile`: to use python code based in the logic of the original java .jar executable file. (CapabilityEvaluator) (RECOMMENDED - fastest option)
    - `jarCPABE`: to use CP-ABE (.jar executable JAVA) (slowest option -Change if certsFiles is not working until the issue is solved)


# Installation / Execution.

After the review of [config.cfg](./config.cfg) file and [docker-compose.yml](docker-compose.yml) files, we are going to obtain then Docker image. To do this, you have to build a local one, thus:

```sh
docker-compose build
```

Finally, AFTER REVIEW docker-compose.yml file and especially its environment variables, to launch the connector image we use the next command:

```sh
docker-compose up -d
```

# How to monitoring.

- To test if the container is running:

```sh
docker ps -as
```

The system must return that the status of the PEP-Proxy container is up.

- To show the PEP-Proxy container logs.

```sh
docker-compose logs pepproxy
```

# Troubleshooting

**If the certificates were renovated and "pep_protocol"="https", you need to refresh them in pepproxy service. Once certificate files are ubicated at the corresponding folder following prerequisites indications, access to the project directory and run:**

```bash  
docker-compose restart pepproxy
```

To solve it automatically, you must add something like this in the crontab, to restart the component each 4 hours:
```
0 0,4,8,12,16,20 * * * cd {{projectFolder}}; docker-compose restart pepproxy
```

**RECOMMENDED, monitor each minute (using the GET endpoint of PEP-Proxy), through crontab, if PEP-Proxyis running:**

```
#To monitor PEP-Proxy.
* * * * * cd {{projectFolder}}; ./monitor_PEP-Proxy.sh >> nohup_PEP-Proxy.out 2>&1
```
*NOTE:* Before configure it, be sure to define:

- {{Protocol}}://{{IP}}
- {{projectPath}}

**RECOMMENDED, to prevent PEP-Proxy logs from filling up the hard drive include in crontab to clean the logs periodically. --> Logs stored in the container of PEP-Proxy.**

```
#To remove PEP-Proxy logs.
0 0 * * 6 cd {{projectFolder}}; cat /dev/null > pepproxy.log
```

**RECOMMENDED, to prevent PEP-Proxy logs from filling up the hard drive include in crontab to clean the logs periodically. --> Logs stored by docker technology related to sercices.**

**NOTE:** This script remove the logs of all dockerized services present in the machine not only the logs of PEP-Proxy.

```
# Assign permissions
sudo chmod 755 deleteLogs.sh

# Copy to /usr/bin
sudo su
cd /usr/bin/
cp {{projectFolder}}/deleteLogs.sh ./

# Configure schedule in /etc/crontab
cd /etc
vi crontab
  # Add this line
  0 0     * * 6   root    /usr/bin/deleteLogs.sh
```

# PEP-Proxy REST API

The PEP-Proxy component supports multiple REST APIs. This section defines all REST APIs supported by PEP-Proxy. In this sense:

- Accesing to resource
- Test PEP-Proxy is running

## Accesing to resource

The PEP-Proxy component hasn’t a specific API, it uses the same API that will forward requests. The unique difference is you must add a new header `x-auth-token` where capability token must be included as a string.


## Test PEP-Proxy is running

PEP-Proxy offers a HTTPS-GET endpoint to test if Capability Manager is running.

```sh
GET / HTTP/1.1
Host: <pep_host>:<pep_port>
Accept: application/json
```

This endpoint can returns the next Status Code:
- `200 - OK`: Component is running.

# Integration with CP-ABE, cipher/decipher data

## Cipher method

At the moment, NGSIv2 and NGSI-LD APIs support cp-abe cipher method. 

Example CPABE cipher:

```sh
java -jar cpabe_cipher.jar "att1 att2 2of2" "hello"
```

We are going to see and example of each supported API to explain this:

* NGSIv2.

    - Default config.cfg configuration.
        
        ```sh
        ...
        APIVersion=NGSIv2
        ...
        relativePathAttributeEncriptation=[[["metadata/encrypt_cpabe/type","policy"]]]
        noEncryptedKeys=["id","type","@context"]
        ```


    - Format to detect an attribute must be cyphered.
        ```sh
        "attrName": {
            "value": attrValue,
            "type": attrType,
            "metadata": {
            "encrypt_cpabe":{
                "type":"policy",
                "value":"policyValue"
            }
            }
        }
        ```

    - How cypher an attribute with an example. 
        - Before cypher process.

            ```sh
            "temperature": {
                "type": "Float",
                "value": 23,
                "metadata": {
                "encrypt_cpabe":{
                    "type":"policy",
                    "value":"att1 att2 2of2"
                }
                }
            }
            ```

        - Cypher operations. 

            ```sh
            temperature["type"]: "cyphertext"

            temperature["value"]: java -jar cpabe_cipher.jar "att1 att2 2of2" "23"

            temperature["metadata"]["encrypt_cpabe"]["value"]: java -jar cpabe_cipher.jar "att1 att2 2of2" "Float"
            ```

        - After cypher process. 
        
            ```sh
            "temperature": {
                "type": "cyphertext",
                "value": "eyJ2YTAvZ3NWMHV2SktLUi9UeFk4Mk13PT0iOiJBQUFBZ0NxK2MycjhMUjMzM1d4SEZwdWVlWGQ5dDdURUdZUXJLUVJuWUFvR2dDQ2NyU09oeXlDdzN0M2kydXo1QlhFcmFWK0Q2T3V0NHFqVVdRcnhabC9pRy94U1VjMldQN3lCSm0weHdPNzk4Y3VjTkJhUGZxeWd1bXlreFJRN254ZWRpaGxPUGphTE85cXZKVzJRRGhYMTdXVXZqT2IxZ0VRRjJ3WGNNMDdlZnhBY0FBQUFnSW80b3hhNEs1WVdGOVk4emQ5Wkg1ZmVUY25JR1l0QmphKysvUnZZVG96ZmFSVHFlQlBMbDdvZVlneGJtd3VpWjVabEZ6WXlkWEdkWkRtb0xjdDIvT1NkSFhiK0hoZzVJMW1pc1Z5UkdaRVlIc3FtcXhLVVlBVmg3aUlvS3lhcXBKYVB3MVR4QWdXMkYwdHBxbWYwSEx5L0dzU3hrU1VuQWFvVmhzRUY2ckRHQUFBQUFnQUFBQUlBQUFBQkFBQUFBQUFBQUFSaGRIUXhBQUFBZ0tZc280aHkxcDhsRlArNGNXUEVrTFRRUERVblhHZVR4OTB5VkhtSmZtQTRoWlJaWlJwZXIzNzYyOUU4Zy8zVkFLZTI3TmxkOTAzdGIyOGp3cXg5SCtDaDJibmNEQTh2T3JEYnlzaEJVcEVJVHZvZGIxa1pFSjdJOE5rUWxUZ1c3c1BIRjhPVTUvVjVyTDRrMXdVQSttSVhHbXhQUFpQQW5KMnVDckpXSFB0eEFBQUFnRGhRVGQ4ZlA0SnJCdldvS1dBc1BXUWZsUVNPZlNYQmZWS2RrR1dNeVlONkFnby9BK1VPT1dIb29lSnFuZDVtMC94bDNGYTdtSGVqWEw1SnlRM0hXbWxzQVV4dlZyczBrT1ZwR2NDSGd3WGF4ODNVOU9qaVFjdTVoamJmVjRGak84Yy9nWlV4U0xKNTBOUzF1U2dzeFZFaXhTcFYxWkRhZkNNZXBwY3lWelVGQUFBQUFRQUFBQUFBQUFBRVlYUjBNZ0FBQUlCOTlJQ1Z5eWFrSldTaW9TN0VHMHpNUTZ0QzlUYll1c0FjMlIxT0VGQktyeUtHZWNicUNyRnRUKzhENXpEeE0zV3dYMlV2TE5qOTB5Zk9qZzFGakljRE1yUlllcU4xS2pQU3RBZEhpeFFHMEdNaEcyYVZwbjgvYzJpUUhqS1JuR0h4ZXFTaCtKUjZSOGVmWXhaOHhNay8rMDdpSDhRVWdkbnRwalNGTE13TURBQUFBSUFCVHUyU1NGLzI2VWxuSzRnbGE3SEZwdUFkQ0UzS0VQSmtBRCs0SjQwbWlXK2hCT2xGazdUQ2pnSDhGVHY3Vkl2WHlNMFgvRE9pc3J5cVN2UFhXZ3hGRHVNZTVVaWdCN29xQkdtcG9pdFlQZmhLQkRyeWN1SGVwYm4xZXhaRjlnb2M3cTQ2REdtOUVHUlIzcWZSYmFKcGlLK2ZEazNUekRsUEFXK21FOWpGZnc9PSJ9",
                "metadata": {
                    "encrypt_cpabe": {
                        "type": "policy",
                        "value": "eyJPWjNBWC9HRU8yTGtZZ0NBU0EvM3lRPT0iOiJBQUFBZ0lVbTRLRlpDakNZSWpmL2kySVpDdUx2WENkdkhxcG1lNGpPeHQwRFlmSnB0cGl4SWRaTGJ5Q2ozQzdtZjl0Z2FERm92TnJmcmFYSzhOeThDWUVCbzh0NHBXT3R6N0R4ZUZkd2RpSkVkTGdOdkxEbzhKMDhRZ3JMM1lxSEcySUJuQjhDNU11eDdiZzEwTkhJU3R6WCtIaHFGV0dNb25xVHpZTThnakU5R3R3ZEFBQUFnSVk0NGo3ZHZaNkFOSWhJTGdMelZ2bE5iQ3FSbk9PVlJhR2tVdmlEVlA0cVNDeWE5V1ZBbEhsbm1XSmgwbm5hUndUODFvQndiRUR1SkNpbko4NDBnajJDQm5vSktwK3U2NjF4MjVEL0c3TzNPN0Q3UkRTS1FmTjRhTWVOOWc3a1k0NmFCY3pNd29kZnY2b2FmOS9SOXkrWjZSa3IzNzFVK2dhSUFSMVFaUVBTQUFBQUFnQUFBQUlBQUFBQkFBQUFBQUFBQUFSaGRIUXhBQUFBZ0J4QjFjWktWSURzRFZqOExmRnEwazNMcXVXcS9IV1daWFBDK1d3UE04cjRGemtMNVNNVWhFTWROZ1AwQm5FTnhCcllvbHlBMnVmWG9EbGJHK3RxL0J3Q05lNUpSWkQybTVGaDd6aFhONTNrT3pyUXpLaERTR1BNZXp3bDhNZENOcmtyU1FxUmxkdFJlVkFpNXVoN0tMK3VkTUxaUmJEeHQzUjFTNmQrRnRaNkFBQUFnQjU2YXRQclU3SjV6WVJoNUYvVWNPSkg4TDJUcExndnhCa1IxTmlhVUo4ajBjVTBGQ09EaVEvYlo4VzFMbldKck5TMEFPL3pHN0c1RjlJM1c2YktQYWhRYWRwbGFyTkcxZmgzMTVmeDRlZTZaRitxUTBQd0l2cUM5K2VrQTRPN3JNR2Fyb2NHaHQxNXdEUUI4QmkzSHRXZ2k2SERhd2NLUmhQQ004S1dZeW16QUFBQUFRQUFBQUFBQUFBRVlYUjBNZ0FBQUlBL05kOXgwQVNCbXBOTk8yaFZITkdrV3hLWU9YN2dkT3l6T0xNY1lBRXUweUZFTUgrbldqSTVFQWZQT0F1bjNLWDhOVERQMG9ua2tuYytGakFudHRraUkrYXlkUURncWRkMFR0TnFyV29MNjlSVEhLV3kya0RTR1JvajJmVmIxcDYrcUVaYzliWXRsVGdaaXY2eVpaZXJhTGxVNURtNlN1YzVaczZkR0dzNnJnQUFBSUJiR2Zrc1ZyTE0vZHBzWkJIZXJ3VFhZd1RwdG1FdE16bzIzZ2tGYXcrczNoejJiN2E1TGg5UWlMaGpLSDVOUUY4ZWZVdTdMbDJBdmdNOGd2YmhWWmluWDRYdlVrbk5LSXpnbFdpZ2lKZEJVUWp0NjJ3Z3poVDluOVlXcnhGd0tydHRvUXJCakZvQXNhZlMwV2dDZzJrYTltc0psbVlXNW9HRU9DV1ZHb09POUE9PSJ9"
                    }
                }
            }
            ```

* NGSI-LD.

    - Default config.cfg configuration.
        
        ```sh
        ...
        APIVersion=NGSILDv1
        ...
        relativePathAttributeEncriptation=[[["encrypt_cpabe/type","Property"]]]
        noEncryptedKeys=["id","type","@context"]
        ```

    - Format to detect an attribute must be cyphered.

        ```sh
        "attrName":{
            "type":"Property",
            "value":"attrValue",
            "encrypt_cpabe":{
                "type":"Property",
                "value":"policyValue"
            }
        }
        ```

    - How cypher an attribute with an example. 
        - Before cypher process.

            ```sh
            "brandName":{
                "type":"Property",
                "value":"Mercedes",
                "encrypt_cpabe":{
                    "type":"Property",
                    "value":"att1 att2 2of2"
                }
            }
            ```
        - Cypher operations.         

            ```sh
            brandName["value"]: java -jar cpabe_cipher.jar "att1 att2 2of2" "Mercedes"
            2. brandName["encrypt_cpabe"]["value"]: "encrypt_cpabe"
            ```

        - After cypher process. 
            
            ```sh
            "brandName":{
                "type":"Property",
                "value":                  "eyJTd3FRZVFnYmN1SUhjMmlwWW5ud3BBPT0iOiJBQUFBZ0tScCtFREZSdi9HeStKaE1KU0JaeldJc3BNNDdXRnl6OEFYdjdGQWdIVmdLdXNlbW9qSExJVWx6ZFpDQXVtTktrV2QxMlhYWW93Mk1PeEtNeEZLemQxcG54ZFR1QStMSzkvdS8yUGNrdG1vOFkvOUZZcSsyMUx2S3hidEo3R2ZHVkpXbE9KL2FMeWhEZmpjYWNvTG9yMHF0MU9pNjBEeWFYYWtKeWcwb3JQM0FBQUFnSjBtKzNuWVVIenhVRE1kcndYSWIwZUdrR0tyeTFOYjh1cmVBc2ZyUnY3ODFoQ0dET3B1WHpUdUdWMGNXcHhoL3o2YWIwQXBTTFNyWS9jb2U1bWdKWjRsUGljKzlNa1Fwc0VYYW1FMnk0VkZCZ1BWOFE2eXJ6QUhlR1RnSXpWSWppb0FhcGxlS3k2WTNLNG94NGtLbm5UUkRFYlVGaVhhSjI4b21MaEJqbmVTQUFBQUFnQUFBQUlBQUFBQkFBQUFBQUFBQUFSaGRIUXhBQUFBZ0lBUHY1N0tNMXhGeXB0S2VUZFM0Y1BNTlg3Nk9saEVJVjFwOVZSUk5VemhYNXhUVVRGTkpEOUFlM3FHYVNQZDA3UzAybjh2bEhKdXM3Q0Yxcm50eU9BWCsrdUU3Z3NuNk5rbm0rM3BiQVZrd01GQlVoRmVwbVBxMGJZZm9XRHkycDlGM2dzUGx2eTFWaVZ0VXM0N0FJVjg1YXNrMXRCOUpnS2hVYVhLekJON0FBQUFnR2hUd1NJVFBNV1ZHMFFXbVpGNysrdHVnNXlBZkNlaExSSFhiTzk0QzRkU0RkTS82T2lCL09IRmFqVnVQd3NoUDQ4ckdYMzNLRVhySXdDeGl3aHYrWTQzWXhadFRMZ2dBK3ZiQS9xa3RnMThpVjl2NFhWR0JKZEVlbHpLRG5yYzNKdjVhSTRZVFE5UnZ3L1p0aFZ1ekkzcnE0Vnl2VGhGbXRrbVFsUnN1VFdpQUFBQUFRQUFBQUFBQUFBRVlYUjBNZ0FBQUlCeFUrVHIwT0J0QXBUelJvYzExWlZ4eGFmNVBFRzlTN09kSmNETDlMb0REUWdtTmRyOU02MFZyTUVJeWZ4RXhLR2RsbEJDc2ROZ0Z0RUQrL283SjFIR25KNk8yQjdVMTcrcmxoZTg2bDFGMGtxem5aTEY4eGw0RGdmYUxoR0RtdmJNdmVNTnlGRUtTK0gvcEdpUWZFMzBaNlJab0xvdnBXTnBoOWUyUk9FVVBRQUFBSUNmamRmNzFmV3E1MzVtZEIwOG9wNmlNeTArWnA1YVUvWGIwRnMyanFKYUZkZFpiV3R4WHpycHlDb1U4RmxpaGxhTVFzRWJJdUt0WWlQQVUzcnNrSjBsaElzSlN3L05UODhUOERSNmhZZ3B5c21DcUFiSWhmWDZubGRPdjN5RldJTmdsL0NscVVUMGxBUzB6bVoweHZMYnhpUG9GNFF0M2laYXVabzR1dDJ2elE9PSJ9",
                "encrypt_cpabe":{
                    "type":"Property",
                    "value":"encrypt_cpabe"
                }
            }
            ```

## Decipher method

Example CPABE decipher:

```sh
java -jar cpabe_decipher.jar "eyJ4azZSeUVIazVMZG4yL2E1ZnA0SGt3PT0iOiJBQUFBZ0gwMXR5dXpMYVZmNGJKV0llQlNpNDlTUnhsMjVOU3RtSGpEUVVxQThJdzVhSm9remxVdk5Rc080dVRZMnd0NkpRZVdiVFhLY3lTREZzU245MXV6NTZSNk9ncWF2eDNYQ0NnazRkRVNOTlhOaFJsWjFpVWJuVWRyaGQvZURHNTJ3WXhVdVBDc1hCZStvM2dGVXpKL2ZmbVY0TXVUTk5vNmNMNHF4MTBvOC91TkFBQUFnR05YV3ljanAwMktscmh4eUZ4TTRxaGs5UFJjRkppVHI1RU4yQjEreDN2TGhwTnhKZGwxWFJlbHYzcldtTTVYTXdnOUh1ZGRBWmR2RTRVWVh4T3dLS0FySzFSVzZJWndTeGc0THN3dmI5UllrZ25wUWNsWFg1UjRQRDEzZXFOblZOVHBwYjFXZ0hkNk9rcG9EMGNRL2xQajYzYTdwekF5a05lYU5FZEUyeHRhQUFBQUFnQUFBQUlBQUFBQkFBQUFBQUFBQUFSaGRIUXhBQUFBZ0JqZndsRzhMMGMvb1pXNVJJaDhVUk54TWFNNE1wMGZkc2hET3lxaGZTWjVaM1NjWTdFekdqNm9NZFFNQkdPb0hWSm1tcVRTcDNicExGM0VEK3pOa2N1WFpJSElSS2d3WGRpbElDNDVoSjNFNmE2Q3Q5SWlzV2ZRSHVhMFREVlFSYlN1ODlZSnQ5T3IybXQ0L3NmcUJjeUdLZ0dLNzVUNnJQcWpBNEpHcnBjNUFBQUFnQW5lbVVRQXJzam5lM293RjJvZE4wb1dTdW1HeFlaemVmSWtIMTRoKy9XalkybFptWVMycmhlRmhmNktORmxmTEpYMUdrTTJWS05QNElrMVlTNEZsbHRiaDgrQmhoa2MwN05qYSsxU0U1Q3VZUFQvMVRZYlp4SWc0ZmM3Q0xiY1pnYUtuQk5ZYVhHd09nY1FSZU1nclpFVnpGZkdvckdhK1MwdVYwY3hGNUZiQUFBQUFRQUFBQUFBQUFBRVlYUjBNZ0FBQUlCNUhybnJTcmRlNWlNTXhVUk55a3VsWXFBbTBVSit6eFJ2LzhYazNOSU96d2ptdHlLaElzdjlDZTJicnZYdnNBenpzbHdURHZYbitKZEJEdFdMSXpuT1NGa2ZKQ1I1WjRxZWI0UW9DQVBBdEpObEYyYy9NRk1BbDhmY0tlaTZ0Z29Fb1UzSnF4T1lkWkp0VllEMVF2UUxObW1xM1kxS1dZQkp2MVp6N1I3R3BRQUFBSUJDbVBONXl0QXZZN2t1NE5UUzVVaXJVWi9HeE5tSGVFLzNMbnpqYkRYTDhoKythN0pwYmNEZTRyM1U2dGZSZ2JNOWdIWVpKZ3htQXE4eFdpOXhlSTdtVGZROUozMW90d1dnaE1nZzFBbngySWxvVG9pcHhRWkoxVlVrbXQxOUhWek1qOUtNbG1iaDV6U0ErVGFwdmd6bWUvVjZBdEdhbFZWVERMeWVUTHJ0Q2c9PSJ9"
```

If we decipher the previous example, only the policy value can't recovered from the original body.

# PEP-Proxy examples request.

Once we obtain the capability token from Capability manager web service, we can send NGSI request adding the "x-auth-token" header to it.

* NGSIv2 example.

    ```sh
    curl -X POST \
    https://<pep_host>:<pep_port>/v2/entities \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -H 'Fiware-Service: room' \
    -H 'Fiware-ServicePath: /pruebaroom' \
    -H 'x-auth-token: {   "del": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvGZqLGhCbTcdBFlTAMZiAvbshtG4DZMC5A0DJ3SNjulniMXBWQMAJm/J3P98T2nE+k/m7n6aJJSdLO+hq1/EICB+kCRDxRbxRhvMh4g+/C2hKiDiDvXNUJTfie4otEbWJYIEW0C1gqxhnw16y63zhh+gJ7WGxQLuSRBmZBA8tmRqQH2L7tZPYZ2CzTqJsE9PZyXTPJVye8LglGOM71/BLvz6Vdjcmu9efPnCm9yXksba7zfSASnCDQf4uBYnC2KXb/LYZedGMcWw0BdEK84Ep8d1rccvzQ7nfxGviYG6M0r/cKLHb58kSxVhwpTNoPg3LCo/2fp0fFb5xvY3n7bR5wIDAQAB",   "id": "knf3jrn1bi6ftt8pf0676ehm4a",   "ii": 1568197484,   "is": "capabilitymanager@um.es",   "su": "joseluis",   "de": "*",   "si": "HdncBHHoNQszE/qK4FzO6+D3s+9KbAnqWUO3xjRzgdRhm9IhcxAfi+6hlx/qZWfufqjKf66KwWV/et5QUe3wWM9kkwk0Nfb1fdG2kyoYM3YHHQZ8k3xeEwoWsR7+MfLtns3BWncHAxtiNSzSupDdpxzijsHBPDx0XUEd+TCpa3KR17WBHSfVHwc1jYPllvcSZfwSlZXbTFmYbpR+P4CNTnfwNr9rnnIB2msK3m7QQQBq5RkutTkeLkP7f8YERyax4n1JFDrQQ9ytMcp/+nZjjrkfOOhZPA1aYkf7f88+qZiKCSNhisxS32NzVuzWtQFhaciYwo1K6R+a9fZyJpeoOQ==",   "ar": [     {       "ac": "*",       "re": "*"     }   ],   "nb": 1568197484,   "na": 1568201084 }' \
    -d '{
        "id": "Room2",
        "type": "Room",
        "temperature": {
            "value": 23,
            "type": "Float",
            "metadata": {
                "encrypt_cpabe":{
                    "type":"policy",
                    "value":"att1 att2 2of2"
                }
            }
        },
        "pressure": {
            "value": 720,
            "type": "Integer",
            "metadata": {
                "encrypt_cpabe2":{
                    "type":"Text",
                    "value":"admin"
                }
            }
        },
        "color": {
            "value": "Red",
            "type": "Text",
            "metadata": {
                "encrypt_cpabe":{
                    "type":"Text",
                    "value":"att4 att5 2of2"
                }
            }
        }
    }
    '
    ```
    
* NGSILDv1 example.    

    ```sh
    curl -X POST \
        https://<pep_host>:<pep_port>/ngsi-ld/v1/entities \
        -H 'Accept: application/ld+json' \
        -H 'Content-Type: application/ld+json' \
        -H 'x-auth-token: {   "del": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvGZqLGhCbTcdBFlTAMZiAvbshtG4DZMC5A0DJ3SNjulniMXBWQMAJm/J3P98T2nE+k/m7n6aJJSdLO+hq1/EICB+kCRDxRbxRhvMh4g+/C2hKiDiDvXNUJTfie4otEbWJYIEW0C1gqxhnw16y63zhh+gJ7WGxQLuSRBmZBA8tmRqQH2L7tZPYZ2CzTqJsE9PZyXTPJVye8LglGOM71/BLvz6Vdjcmu9efPnCm9yXksba7zfSASnCDQf4uBYnC2KXb/LYZedGMcWw0BdEK84Ep8d1rccvzQ7nfxGviYG6M0r/cKLHb58kSxVhwpTNoPg3LCo/2fp0fFb5xvY3n7bR5wIDAQAB",   "id": "knf3jrn1bi6ftt8pf0676ehm4a",   "ii": 1568197484,   "is": "capabilitymanager@um.es",   "su": "joseluis",   "de": "*",   "si": "HdncBHHoNQszE/qK4FzO6+D3s+9KbAnqWUO3xjRzgdRhm9IhcxAfi+6hlx/qZWfufqjKf66KwWV/et5QUe3wWM9kkwk0Nfb1fdG2kyoYM3YHHQZ8k3xeEwoWsR7+MfLtns3BWncHAxtiNSzSupDdpxzijsHBPDx0XUEd+TCpa3KR17WBHSfVHwc1jYPllvcSZfwSlZXbTFmYbpR+P4CNTnfwNr9rnnIB2msK3m7QQQBq5RkutTkeLkP7f8YERyax4n1JFDrQQ9ytMcp/+nZjjrkfOOhZPA1aYkf7f88+qZiKCSNhisxS32NzVuzWtQFhaciYwo1K6R+a9fZyJpeoOQ==",   "ar": [     {       "ac": "*",       "re": "*"     }   ],   "nb": 1568197484,   "na": 1568201084 }' \
        -d '{
        "@context":[
                "https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld",
                {
                    "Vehicle": "http://example.org/vehicle/Vehicle",
                    "brandName": "http://example.org/vehicle/brandName",
                    "speed": "http://example.org/vehicle/speed",
                    "color": "http://example.org/vehicle/color"
                }
        ],
        "id":"urn:ngsi-ld:Vehicle:99",
        "type":"Vehicle",
        "brandName":{
            "type":"Property",
            "value":"Mercedes",
            "encrypt_cpabe":{
            "type":"Property",
            "value":"att1 att2 2of2"
            }
        },
        "speed":{
            "type":"Property",
            "value":80,
            "encrypt_cpabe2":{
            "type":"Property",
            "value":"admin"
            }
        },
        "color":{
            "type":"Property",
            "value":"Red"
        }  
        }'
    ```

# Integration with blockchain - APIS

Optionally, PEP-Proxy supports blockchain integration. If Blockchain integration is configured, PEP-Proxy access to Blockchain to obtain Capability Token status to validate if it was revoked.

![security-authorisation-PEPBlockchain](./img/plantuml/security-authorisation-PEPBlockchain.png)

-
This component supports different blockchain APIs. These APIs can be selected by defining the corresponding value in the "blockchain_api" parameter. In this regard, it can be select one of the following values:

- `NativeAPI`: Uses the custom API defined in the first version of the integration. It considers a non-standard format representation of the concept stored in the Blockchain.
- `NGSIv2`: Uses the NGSIv2 data format representation which is assumed by Blockchain's Smart Contracts.
- `NGSI-LD`: Uses the NGSI-LD data format representation which is assumed by Blockchain's Smart Contracts.

Other relevants parameters are:

- `blockchain_get_token`: Resource to obtain a the capablity token entity from Blockchain.

    - `/token/` --> For NativeAPI.
    - `/chain/events?entityid=` --> For NGSIv2.
    - `/chain/events?entityid=` --> For NGSI-LD.
        
The following subsections will detail the features/differences of these supported APIs related to the functionality of this component. The Postman Collection file ([Integration-DcapBAC&DLT-OdinS.postman_collection.json](Integration-DcapBAC&DLT-OdinS.postman_collection.json)) contains, between others, an example of these requests.

## NativeAPI

1. GET TOKEN:

    - Request:

        ```
        GET /token/{{capToken}} HTTP/1.1
        Host: localhost:8000
        ```

    - Response:

        - `200`: If the capability token is registered.

            - Content-Type: text/plain; charset=utf-8(compatible with Content-Type: application/json) 

            - Body: {"state": 1}

              If  {"state": 0} the the token is revoked.

2. REVOKE TOKEN:

    - Request:

        ```
        POST /token/revoke HTTP/1.1
        Host: localhost:8000
        Content-Type: text/plain
        Content-Length: 35

        {"id":"{{capToken}}"}
        ```

    - Response:

        - `200`: If the capability token was revoked.

## NGSIv2

1. GET TOKEN:

    - Request:

        ```
        GET /chain/events?entityid={{capToken}} HTTP/1.1
        Host: localhost:8000
        Accept: application/json
        ```

    - Response:

        - `200`: If the capability token is registered.

            - Body: {"state": 1}

                If  {"state": 0} the the token is revoked.

2. REVOKE TOKEN:

    - Request:

        ```
        POST /chain/publish HTTP/1.1
        Host: localhost:8000
        Content-Type: application/json

        {
            "id": "urn:ngsi-ld:blockchaincaptoken:{{capToken}}",
            "type": "blockchaincaptoken",
            "host": {
                "type": "Property",
                "value": "172.19.0.1"
            },
            "state": {
                "type": "Property",
                "value": "0"
            },
            "timestamp": {
                "type": "Property",
                "value": "2022-11-28T15:46:50.672Z"
            },
            "token": {
                "type": "Property",
                "value": "{%22id%22: %22gljtm42m61sck5sr1k3b8jkums%22, %22ii%22: 1669649410, %22is%22: %22capabilitymanager@odins.es%22, %22su%22: %22admin%22, %22de%22: %22http://localhost:1027%22, %22si%22: %22MEQCICy3o1nsJF1qwfyP5Zzqv5MZPC3xFxT912mCfJrqJ/9pAiB0sYEHUTBOhop2nrTc1G0SLrKY7F2S7EdRAAwT/OnzLw%3D%3D%22, %22ar%22: [{%22ac%22: %22GET%22, %22re%22: %22/v2/entities.*%22}], %22nb%22: 1669650410, %22na%22: 1669660410}"
            }
        }
        ```

    - Response:

        - `200`: If the capability token was revoked.

## NGSI-LD

1. GET TOKEN:

    - Request:

        ```
        GET /chain/events?entityid={{capToken}} HTTP/1.1
        Host: localhost:8000
        Accept: application/json
        ```

    - Response:

        - `200`: If the capability token is registered.

            - Body: {"state": 1}

                If  {"state": 0} the the token is revoked.

2. REVOKE TOKEN:

    - Request:

        ```
        POST /chain/publish HTTP/1.1
        Host: localhost:8000
        Content-Type: application/json

        {
            "id": "urn:ngsi-ld:blockchaincaptoken:{{capToken}}",
            "type": "blockchaincaptoken",
            "host": {
                "type": "Property",
                "value": "172.19.0.1"
            },
            "state": {
                "type": "Property",
                "value": "0"
            },
            "timestamp": {
                "type": "Property",
                "value": "2022-11-28T15:46:50.672Z"
            },
            "token": {
                "type": "Property",
                "value": "{%22id%22: %22gljtm42m61sck5sr1k3b8jkums%22, %22ii%22: 1669649410, %22is%22: %22capabilitymanager@odins.es%22, %22su%22: %22admin%22, %22de%22: %22http://localhost:1027%22, %22si%22: %22MEQCICy3o1nsJF1qwfyP5Zzqv5MZPC3xFxT912mCfJrqJ/9pAiB0sYEHUTBOhop2nrTc1G0SLrKY7F2S7EdRAAwT/OnzLw%3D%3D%22, %22ar%22: [{%22ac%22: %22GET%22, %22re%22: %22/v2/entities.*%22}], %22nb%22: 1669650410, %22na%22: 1669660410}"
            }
        }
        ```

    - Response:

        - `200`: If the capability token was revoked.

# Required backups.

This component does not require any backup process.

# Extra

## Proxy PEP - supporting new API.

To support new APIs, follow the next steps:

1. Add API to config.cfg file.
    - APIVersion: Define a value to the new API and include it also into the param comments.
    - allApiHeaders: Define a new element for the API with the possible headers.
    - allSeparatorPathAttributeEncriptation: Define a new element for the API with the value.
    - relativePathAttributeEncriptation: TODO
    - noEncryptedKeys: TODO

2. Add a new file into API folder named "Utils{APIVersion}.py" using for it another existing file (copy).

3. Update UtilsPEP.py to import the new file and add a new case in each if statement.

4. Update API/"Utils{APIVersion}.py" file to add the corresponding functionality for the API.

5. Review/update al coments included in previous 4 points and update the README.md file


## Proxy PEP - supporting new cypher.

To support new cypher, follow the next steps:

1. Add attribute cypher condition to relativePathAttributeEncriptation param (config.cfg file).

2. Update all files of API folder to support the cypher. Exactly you need to review the "cipherBodyAttributes" function and add a new else: if(condition): statement into the for. The condition must validate the corresponding condition established in relativePathAttributeEncriptation param. Finally, include into the if block the code to cypher.


## Proxy PEP - updating relativePathAttributeEncriptation param (config.cfg).

The conditions established in this param determinate if an attribute must be cyphered. The cypher process validates the same conditions before to cypher the attribute. For this reason, if we update relativePathAttributeEncriptation param we must update the code, exactly, the "cipherBodyAttributes" functions into the API folder files.


## Proxy PEP - supporting new actions/path with cypher.

To support it you must review:

1. Edit UtilsPEP.py file and update the encryptProcess function.

2. Edit the file into API folder corresponding with the API and update/review processBody function.

**IMPORTANT:** If request body hasn't the same format as (processCypher example comments), you need build new functions because you can't use processCypher functionality.